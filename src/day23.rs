pub fn part1() {
    let cups = Cups::from("562893147");
    let s = p1(cups);

    assert_eq!(&s, "38925764");

    println!("day23 part1: {}", s);
}

pub fn part2() {
    let mut cups = Cups::from_pt2("562893147");

    cups.step_n(10_000_000);

    let a = cups.cups[1];
    let b = cups.cups[a];

    assert_eq!(a, 871396);
    assert_eq!(b, 150509);

    println!("day23 part2: {} ", a * b);
}

struct Cups {
    // value of 0 points to first element ["cur" pointer],
    // value of 1.. points to next
    cups: Vec<usize>,
}

impl Cups {
    fn from(line: &str) -> Self {
        let mut cups = vec![0; 10];

        let mut prev = 0;
        for c in line.chars() {
            let c = usize::from_str_radix(&c.to_string(), 10).expect("bad digit");
            cups[prev] = c;
            prev = c;
        }

        // last points to first
        cups[prev] = cups[0];

        Self { cups }
    }

    fn from_pt2(line: &str) -> Self {
        let mut cups = vec![0; 1_000_001];

        let mut prev = 0;
        for c in line.chars() {
            let c = usize::from_str_radix(&c.to_string(), 10).expect("bad digit");
            cups[prev] = c;
            prev = c;
        }

        // fill up 10 to 1mio
        for n in 10..=1_000_000 {
            cups[prev] = n;
            prev = n;
        }

        // last points to first
        cups[prev] = cups[0];

        Self { cups }
    }

    // figure out which number is "dest", now
    // (numbers in "take" are not available)
    fn choose_dest(&self, cur: usize, take: &[usize]) -> usize {
        // println!("choose dest for cur {} take {:?}", cur, take);

        let mut test = cur;
        loop {
            test -= 1;
            if test == 0 {
                test = self.cups.len() - 1;
            };
            if !take.contains(&test) {
                return test;
            }
        }
    }

    fn step(&mut self) {
        let cur = self.cups[0];

        // take next three
        let a = self.cups[cur];
        let b = self.cups[a];
        let c = self.cups[b];
        let take = [a, b, c];

        // remove "take" elements from the linked list
        self.cups[cur] = self.cups[c];

        // figure out the new "dest"
        let dest = self.choose_dest(cur, &take);

        // remember which element "dest" points to
        let tmp = self.cups[dest];
        // point from "dest" to first element of "take" (a)
        self.cups[dest] = a;
        // point from "c" to the element that "dest" pointed to
        self.cups[c] = tmp;

        // point head to next after "cur"
        self.cups[0] = self.cups[cur];
    }

    fn step_n(&mut self, n: usize) {
        (0..n).for_each(|_| self.step());
    }
}

fn p1(mut cups: Cups) -> String {
    cups.step_n(100);

    let mut s = String::new();

    let mut next = cups.cups[1];
    while next != 1 {
        s += &format!("{}", next);
        next = cups.cups[next];
    }

    s
}

#[cfg(test)]
mod tests {
    use crate::day23::{p1, Cups};

    #[test]
    fn test1() {
        let cups = Cups::from("389125467");

        let s = p1(cups);

        assert_eq!(&s, "67384529");
    }

    #[test]
    fn test2() {
        let mut cups = Cups::from_pt2("389125467");

        cups.step_n(10_000_000);

        let a = cups.cups[1];
        let b = cups.cups[a];

        assert_eq!(a, 934001);
        assert_eq!(b, 159792);
    }
}
