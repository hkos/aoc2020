use crate::aocdata;

use regex::Regex;
use std::collections::HashSet;

pub fn part1() {
    let data = aocdata::load("data/day08p1.txt");
    let program = Program::from(&data);

    let mut handheld = Handheld::new(program);
    let ret = handheld.run();
    assert!(!ret);

    let acc = handheld.acc;

    assert_eq!(acc, 1684);

    println!("day08 part1: {}", acc);
}

pub fn part2() {
    let data = aocdata::load("data/day08p1.txt");
    let program = Program::from(&data);

    for mutation in program.mutations() {
        let mut handheld = Handheld::new(mutation);
        let ret = handheld.run();

        if ret {
            let acc = handheld.acc;
            println!("day08 part2: {}", acc);
            assert_eq!(acc, 2188);
            return;
        }
    }

    panic!("no result found")
}

struct Handheld {
    prog: Program,
    ip: usize,
    acc: i64,
}

impl Handheld {
    fn new(prog: Program) -> Self {
        Self {
            prog,
            ip: 0,
            acc: 0,
        }
    }

    fn step(&mut self) {
        let op = self.prog.ops.get(self.ip).expect("no op found");
        match op.op {
            Opcode::NOP => self.ip += 1,
            Opcode::ACC => {
                self.acc += op.val;
                self.ip += 1
            }
            Opcode::JMP => self.ip = (self.ip as i64 + op.val) as usize,
        }
    }

    // returns false if repeat instruction found, true if terminated normally
    fn run(&mut self) -> bool {
        let mut visited = HashSet::new();
        while self.ip < self.prog.ops.len() {
            visited.insert(self.ip);

            self.step();

            if visited.contains(&self.ip) {
                return false;
            }
        }

        true
    }
}

#[derive(Clone, Debug)]
struct Program {
    ops: Vec<Op>,
}

impl Program {
    fn from(data: &[String]) -> Self {
        let ops = data.iter().map(|line| Op::from(line)).collect();

        Self { ops }
    }

    fn mutations(&self) -> Vec<Self> {
        let mut res = vec![];
        for (i, op) in self.ops.iter().enumerate() {
            if op.op == Opcode::ACC {
                continue;
            }

            let mut cloned = self.clone();
            match &cloned.ops[i].op {
                Opcode::NOP => cloned.ops[i].op = Opcode::JMP,
                Opcode::JMP => cloned.ops[i].op = Opcode::NOP,
                _ => panic!("unexpected opcode"),
            }

            res.push(cloned);
        }

        res
    }
}

#[derive(Clone, Debug)]
struct Op {
    op: Opcode,
    val: i64,
}

#[derive(Clone, PartialEq, Debug)]
enum Opcode {
    NOP,
    ACC,
    JMP,
}

impl Opcode {
    fn from(op: &str) -> Self {
        match op {
            "nop" => Self::NOP,
            "acc" => Self::ACC,
            "jmp" => Self::JMP,
            _ => panic!("unexpected opcode {}", op),
        }
    }
}

impl Op {
    fn from(line: &str) -> Self {
        lazy_static! {
            static ref OP_RE: Regex = Regex::new(r"^([a-z]{3}) ([+-]\d+)$").unwrap();
        }

        let cap = OP_RE.captures(line).unwrap();

        let op = cap.get(1).unwrap().as_str();
        let val = cap.get(2).unwrap().as_str();

        let op = Opcode::from(op);
        let val = i64::from_str_radix(val, 10).unwrap_or_else(|_| panic!("bad value {}", val));

        Self { op, val }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day08::{Handheld, Program};

    #[test]
    fn test() {
        let data = aocdata::load("data/day08ex1.txt");
        let program = Program::from(&data);

        let mut handheld = Handheld::new(program);
        let ret = handheld.run();

        assert!(!ret);
        assert_eq!(handheld.acc, 5);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day08ex1.txt");
        let program = Program::from(&data);

        for mutation in program.mutations() {
            let mut handheld = Handheld::new(mutation);
            let ret = handheld.run();

            if ret {
                assert_eq!(handheld.acc, 8);
                return;
            }
        }

        panic!("no result found");
    }
}
