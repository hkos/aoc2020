use crate::aocdata;

use regex::Regex;

pub fn part1() {
    let data = aocdata::load("data/day05p1.txt");
    let max = seats_list(&data).into_iter().max();

    println!("day05 part1: {}", max.expect(""));
}

pub fn part2() {
    let data = aocdata::load("data/day05p1.txt");

    let mut sead_ids = seats_list(&data);
    sead_ids.sort_unstable();

    // find the first "gap" in the (now ordered) list of sead IDs
    for i in 0..sead_ids.len() - 1 {
        if sead_ids[i + 1] - sead_ids[i] != 1 {
            println!("day05 part2: {}", sead_ids[i] + 1);
            break;
        }
    }
}

#[derive(PartialEq, Debug)]
struct Seat {
    row: usize,
    col: usize,
}

impl Seat {
    fn decode(seat: &str) -> Option<Seat> {
        lazy_static! {
            static ref SEAT_RE: Regex = Regex::new(r"^[FB]{7}[RL]{3}$").unwrap();
        }
        if !SEAT_RE.is_match(seat) {
            return None;
        }

        let (row, col) = seat.split_at(7);

        let row = row.replace('F', "0").replace('B', "1");
        let col = col.replace('L', "0").replace('R', "1");

        let row = usize::from_str_radix(&row, 2).unwrap();
        let col = usize::from_str_radix(&col, 2).unwrap();

        Some(Seat { row, col })
    }

    fn id(&self) -> usize {
        self.row * 8 + self.col
    }
}

fn seats_list(data: &[String]) -> Vec<usize> {
    data.iter()
        .map(|seat| Seat::decode(seat).unwrap().id())
        .collect()
}

#[cfg(test)]
mod tests {
    use crate::day05::Seat;

    #[test]
    fn test() {
        // BFFFBBFRRR: row 70, column 7, seat ID 567.
        let seat = Seat::decode("BFFFBBFRRR").unwrap();
        assert_eq!(seat, Seat { row: 70, col: 7 });
        assert_eq!(seat.id(), 567);

        // FFFBBBFRRR: row 14, column 7, seat ID 119.
        assert_eq!(Seat::decode("FFFBBBFRRR").unwrap().id(), 119);

        // BBFFBBFRLL: row 102, column 4, seat ID 820.
        assert_eq!(Seat::decode("BBFFBBFRLL").unwrap().id(), 820);
    }
}
