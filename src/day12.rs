use crate::aocdata;

pub fn part1() {
    let data = aocdata::load("data/day12.txt");
    let nav = Nav::read(&data);
    let mut ship = Ship::new();

    nav.run1(&mut ship);

    let dist = ship.pos.manhattan_dist();

    assert_eq!(dist, 962);

    println!("day12 part1: {}", dist);
}

pub fn part2() {
    let data = aocdata::load("data/day12.txt");
    let nav = Nav::read(&data);
    let mut ship = Ship::new();

    nav.run2(&mut ship);

    let dist = ship.pos.manhattan_dist();

    assert_eq!(dist, 56135);

    println!("day12 part2: {}", dist);
}

struct Nav {
    instr: Vec<Instr>,
}

impl Nav {
    fn run1(&self, ship: &mut Ship) {
        self.instr.iter().for_each(|i| ship.handle1(i));
    }

    fn run2(&self, ship: &mut Ship) {
        self.instr.iter().for_each(|i| ship.handle2(i));
    }

    fn read(data: &[String]) -> Self {
        Self {
            instr: data.iter().map(|l| Instr::read(l)).collect(),
        }
    }
}

struct Instr {
    act: Action,
    val: i64,
}

impl Instr {
    fn read(l: &str) -> Self {
        let a = &l[0..1];
        let v = &l[1..];

        let act = match a {
            "N" => Action::N,
            "E" => Action::E,
            "S" => Action::S,
            "W" => Action::W,
            "L" => Action::L,
            "R" => Action::R,
            "F" => Action::F,
            _ => panic!(),
        };

        let val = i64::from_str_radix(v, 10).expect("bad number");

        Self { act, val }
    }
}

enum Action {
    N,
    W,
    S,
    E,

    L,
    R,

    F,
}

#[derive(Debug)]
struct Pos(i64, i64);

impl Pos {
    fn manhattan_dist(&self) -> usize {
        (i64::abs(self.0) + i64::abs(self.1)) as usize
    }
}

#[derive(Clone, Copy)]
enum Dir {
    N,
    W,
    S,
    E,
}

impl Dir {
    fn next(&self) -> Self {
        match self {
            Self::N => Self::E,
            Self::E => Self::S,
            Self::S => Self::W,
            Self::W => Self::N,
        }
    }
}

struct Ship {
    pos: Pos,
    dir: Dir,
    wayp: Pos,
}

impl Ship {
    fn new() -> Self {
        Ship {
            pos: Pos(0, 0),
            dir: Dir::E,
            wayp: Pos(10, 1),
        }
    }

    fn handle1(&mut self, instr: &Instr) {
        match instr.act {
            Action::N => self.pos = self.mv(Dir::N, instr.val, &self.pos),
            Action::E => self.pos = self.mv(Dir::E, instr.val, &self.pos),
            Action::S => self.pos = self.mv(Dir::S, instr.val, &self.pos),
            Action::W => self.pos = self.mv(Dir::W, instr.val, &self.pos),
            Action::L => self.turn(-instr.val),
            Action::R => self.turn(instr.val),
            Action::F => self.pos = self.mv(self.dir, instr.val, &self.pos),
        }
    }

    fn handle2(&mut self, instr: &Instr) {
        match instr.act {
            Action::N => self.wayp = self.mv(Dir::N, instr.val, &self.wayp),
            Action::E => self.wayp = self.mv(Dir::E, instr.val, &self.wayp),
            Action::S => self.wayp = self.mv(Dir::S, instr.val, &self.wayp),
            Action::W => self.wayp = self.mv(Dir::W, instr.val, &self.wayp),
            Action::L => self.rot_wp(-instr.val),
            Action::R => self.rot_wp(instr.val),
            Action::F => self.move_by_wp(instr.val),
        }
    }

    fn rot_wp(&mut self, val: i64) {
        let val = val / 90;
        let val = (val + 4) % 4;

        (0..val).for_each(|_| {
            let x = self.wayp.0;
            let y = self.wayp.1;
            self.wayp = Pos(y, -x);
        });
    }

    fn move_by_wp(&mut self, val: i64) {
        self.pos.0 += self.wayp.0 * val;
        self.pos.1 += self.wayp.1 * val;
    }

    fn mv(&self, dir: Dir, val: i64, pos: &Pos) -> Pos {
        match dir {
            Dir::N => Pos(pos.0, pos.1 + val),
            Dir::E => Pos(pos.0 + val, pos.1),
            Dir::S => Pos(pos.0, pos.1 - val),
            Dir::W => Pos(pos.0 - val, pos.1),
        }
    }

    fn turn(&mut self, val: i64) {
        let val = val / 90;
        let val = (val + 4) % 4;
        (0..val).for_each(|_| self.dir = self.dir.next());
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day12::{Nav, Ship};

    #[test]
    fn test() {
        let data = aocdata::load("data/day12ex1.txt");
        let nav = Nav::read(&data);
        let mut ship = Ship::new();

        nav.run1(&mut ship);

        assert_eq!(ship.pos.manhattan_dist(), 25);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day12ex1.txt");
        let nav = Nav::read(&data);
        let mut ship = Ship::new();

        nav.run2(&mut ship);

        assert_eq!(ship.pos.manhattan_dist(), 286);
    }
}
