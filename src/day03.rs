use crate::aocdata;

pub fn part1() {
    let map = Map::from(&aocdata::load("data/day03p1.txt"));
    let count = Map::trees(map.traverse((3, 1)));

    println!("day03 part1: {}", count);
}

pub fn part2() {
    let map = Map::from(&aocdata::load("data/day03p1.txt"));
    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

    let counts = map.test_slopes(slopes);

    let prod = counts.iter().product::<usize>();

    println!("day03 part2: {}", prod);
}

#[derive(Debug)]
struct Map {
    lines: Vec<Line>,
}

#[derive(Debug)]
struct Line {
    squares: Vec<Square>,
}

#[derive(PartialEq, Clone, Copy, Debug)]
enum Square {
    Tree,
    Open,
}

impl Map {
    fn from(data: &[String]) -> Self {
        let lines = data.iter().map(|l| Line::from(l)).collect();
        Self { lines }
    }

    fn size(&self) -> (usize, usize) {
        assert!(!self.lines.is_empty());
        (self.lines[0].squares.len(), self.lines.len())
    }

    fn get(&self, x: usize, y: usize) -> Square {
        let size = self.size();
        let x_wrap = x % size.0;

        self.lines[y].squares[x_wrap]
    }

    fn traverse(&self, slope: (usize, usize)) -> Vec<Square> {
        let mut res = vec![];
        let mut pos = (0, 0);

        for l in (slope.1..self.size().1).step_by(slope.1) {
            pos = (pos.0 + slope.0, l);
            res.push(self.get(pos.0, pos.1));
        }

        res
    }

    fn trees(squares: Vec<Square>) -> usize {
        squares.iter().filter(|&&s| s == Square::Tree).count()
    }

    fn test_slopes(&self, slopes: Vec<(usize, usize)>) -> Vec<usize> {
        slopes
            .iter()
            .map(|&sl| Self::trees(self.traverse(sl)))
            .collect()
    }
}

impl Line {
    fn from(data: &str) -> Self {
        let squares = data.chars().map(Self::char_to_square).collect();
        Line { squares }
    }

    fn char_to_square(ch: char) -> Square {
        match ch {
            '.' => Square::Open,
            '#' => Square::Tree,
            _ => panic!("unexpected input data"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day03::{Map, Square};

    #[test]
    fn test() {
        let data = aocdata::load("data/day03ex1.txt");
        let map = Map::from(&data);

        assert_eq!(map.get(1, 1), Square::Open);

        let count = Map::trees(map.traverse((3, 1)));

        assert_eq!(count, 7);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day03ex1.txt");
        let map = Map::from(&data);

        let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

        let counts = map.test_slopes(slopes);

        assert_eq!(counts, vec![2, 7, 3, 4, 2]);
        assert_eq!(counts.iter().product::<usize>(), 336);
    }

    #[test]
    fn test_p2() {
        let map = Map::from(&aocdata::load("data/day03p1.txt"));
        let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

        let counts = map.test_slopes(slopes);

        assert_eq!(counts.iter().product::<usize>(), 2122848000);
    }
}
