use std::fs::File;
use std::io::{BufRead, BufReader};

pub fn load(filename: &str) -> Vec<String> {
    let file = File::open(filename).expect("Couldn't load");
    BufReader::new(file)
        .lines()
        .map(|l| l.expect("bad line"))
        .collect()
}

pub fn get_blocks(data: &[String]) -> Vec<Vec<String>> {
    let mut tmp: Vec<String> = vec![];
    let mut res: Vec<Vec<String>> = vec![];

    data.iter().for_each(|line| {
        if line.is_empty() {
            res.push(tmp.clone());
            tmp = vec![];
        } else {
            tmp.push(line.to_owned());
        }
    });

    if !tmp.is_empty() {
        res.push(tmp);
    }

    res
}

pub fn as_u64(data: &[String]) -> Vec<u64> {
    data.iter()
        .map(|l| u64::from_str_radix(l, 10).expect("bad line"))
        .collect()
}
