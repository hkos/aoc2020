use crate::aocdata;
use std::collections::HashSet;
use std::hash::Hash;

pub fn part1() {
    let data = aocdata::load("data/day17.txt");

    let count = p1(&data);
    assert_eq!(count, 252);

    println!("day17 part1: {}", count);
}

pub fn part2() {
    let data = aocdata::load("data/day17.txt");

    let count = p2(&data);
    // assert_eq!(count, 252);

    println!("day17 part2: {}", count);
}

fn p1(data: &[String]) -> usize {
    let mut cube = Cube::<Pos3d>::new();
    cube.init(data);

    for _ in 1..=6 {
        cube = cube.step();
    }

    cube.count()
}

fn p2(data: &[String]) -> usize {
    let mut cube = Cube::<Pos4d>::new();
    cube.init(data);

    for _ in 1..=6 {
        cube = cube.step();
    }

    cube.count()
}

#[derive(Clone, Debug)]
struct Cube<P> {
    space: HashSet<P>,
    max: P,
    min: P,
}

impl<P: Pos + Eq + Hash + Clone> Cube<P> {
    fn new() -> Self {
        Cube {
            space: HashSet::new(),

            // outer bound (actual max may be smaller after unsetting points)
            max: P::zero(),

            // outer bound (actual min may be higher after unsetting points)
            min: P::zero(),
        }
    }

    fn set(&mut self, pos: P) {
        self.max.max(&pos); // expand max
        self.min.min(&pos); // expand min

        self.space.insert(pos);
    }

    fn unset(&mut self, pos: &P) {
        self.space.remove(pos);
    }

    fn get(&self, pos: &P) -> bool {
        self.space.contains(pos)
    }

    fn init(&mut self, data: &[String]) {
        for (y, line) in data.iter().enumerate() {
            for (x, ch) in line.chars().enumerate() {
                if ch == '#' {
                    self.set(P::two_d(x as i32, y as i32));
                }
            }
        }
    }

    fn count_neighbors(&self, pos: &P) -> usize {
        let neighbors = NeighborIter::new(pos);
        neighbors.filter(|n| self.get(n)).count()
    }

    fn step(&self) -> Self {
        let mut next: Cube<P> = self.clone();

        let iter = SpaceIter::new(&self.min, &self.max);
        iter.for_each(|pos| {
            let neigh = self.count_neighbors(&pos);

            match self.get(&pos) {
                true => {
                    if (neigh != 2) && (neigh != 3) {
                        next.unset(&pos)
                    }
                }
                false => {
                    if neigh == 3 {
                        next.set(pos)
                    }
                }
            }
        });

        next
    }

    fn count(&self) -> usize {
        self.space.len()
    }
}

struct NeighborIter<P> {
    cur: P,
    base: P,
}

impl<P: Pos + Eq + Hash + Clone> NeighborIter<P> {
    fn new(base: &P) -> Self {
        NeighborIter {
            cur: P::zero(), // zero is the starting point
            base: base.clone(),
        }
    }
}

impl<P: Pos + Eq + Hash + Clone> Iterator for NeighborIter<P> {
    type Item = P;

    fn next(&mut self) -> Option<P> {
        match self.cur.next_for_neighbor_iter() {
            true => Some(self.base.add(&self.cur)),
            false => None,
        }
    }
}

struct SpaceIter<P> {
    min: P,
    max: P,
    cur: P,
}

impl<P: Pos + Eq + Hash + Clone> SpaceIter<P> {
    fn new(min: &P, max: &P) -> Self {
        // iterate
        let min = min.minus_one();
        let max = max.plus_one();

        SpaceIter {
            min: min.clone(),
            max,
            cur: min,
        }
    }
}

impl<P: Pos + Eq + Hash + Clone> Iterator for SpaceIter<P> {
    type Item = P;

    fn next(&mut self) -> Option<P> {
        if let Some(next) = self.cur.next_for_space_iter(&self.min, &self.max) {
            self.cur = *next.clone();
            Some(*next)
        } else {
            None
        }
    }
}

trait Pos {
    /// a new Pos with 0 in every dimension
    fn zero() -> Self;

    /// subtract 1 from every dimension
    fn minus_one(&self) -> Self;

    /// add 1 to every dimension
    fn plus_one(&self) -> Self;

    /// make a point from a 2d coordinate, with all other dimensions set to 0
    fn two_d(x: i32, y: i32) -> Self;

    /// add two points
    fn add(&self, other: &Self) -> Self;

    /// find the maximum between two points
    fn max(&mut self, pos: &Self);

    /// find the minimum between two points
    fn min(&mut self, pos: &Self);

    /// used in NeighborIter
    fn next_for_neighbor_iter(&mut self) -> bool;

    /// used in SpaceIter
    fn next_for_space_iter(&self, min: &Self, max: &Self) -> Option<Box<Self>>;
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct Pos3d {
    x: i32,
    y: i32,
    z: i32,
}

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
struct Pos4d {
    x: i32,
    y: i32,
    z: i32,
    w: i32,
}

impl Pos3d {
    fn new(x: i32, y: i32, z: i32) -> Self {
        Pos3d { x, y, z }
    }

    fn set(&mut self, x: i32, y: i32, z: i32) {
        self.x = x;
        self.y = y;
        self.z = z;
    }
}

impl Pos4d {
    fn new(x: i32, y: i32, z: i32, w: i32) -> Self {
        Pos4d { x, y, z, w }
    }

    fn set(&mut self, x: i32, y: i32, z: i32, w: i32) {
        self.x = x;
        self.y = y;
        self.z = z;
        self.w = w;
    }
}

impl Pos for Pos3d {
    fn zero() -> Self {
        Self::new(0, 0, 0)
    }

    fn minus_one(&self) -> Self {
        self.add(&Self::new(-1, -1, -1))
    }

    fn plus_one(&self) -> Self {
        self.add(&Self::new(1, 1, 1))
    }

    fn two_d(x: i32, y: i32) -> Self {
        Self::new(x, y, 0)
    }

    fn add(&self, other: &Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }

    fn max(&mut self, pos: &Self) {
        self.x = pos.x.max(self.x);
        self.y = pos.y.max(self.y);
        self.z = pos.z.max(self.z);
    }

    fn min(&mut self, pos: &Self) {
        self.x = pos.x.min(self.x);
        self.y = pos.y.min(self.y);
        self.z = pos.z.min(self.z);
    }

    fn next_for_neighbor_iter(&mut self) -> bool {
        match (self.x, self.y, self.z) {
            (0, 0, 0) => self.set(-1, -1, -1), // start at -1 ^ 3
            (-1, 0, 0) => self.set(1, 0, 0),   // skip zero
            (1, 1, 1) => return false,
            (1, 1, z) => self.set(-1, -1, z + 1),
            (1, y, z) => self.set(-1, y + 1, z),
            (x, y, z) => self.set(x + 1, y, z),
        }

        true
    }

    fn next_for_space_iter(&self, min: &Self, max: &Self) -> Option<Box<Self>> {
        if self == max {
            None
        } else {
            let n = if self.x == max.x && self.y == max.y {
                Self::new(min.x, min.y, self.z + 1)
            } else if self.x == max.x {
                Self::new(min.x, self.y + 1, self.z)
            } else {
                Self::new(self.x + 1, self.y, self.z)
            };
            Some(Box::new(n))
        }
    }
}

impl Pos for Pos4d {
    fn zero() -> Self {
        Self::new(0, 0, 0, 0)
    }

    fn minus_one(&self) -> Self {
        self.add(&Self::new(-1, -1, -1, -1))
    }

    fn plus_one(&self) -> Self {
        self.add(&Self::new(1, 1, 1, 1))
    }

    fn two_d(x: i32, y: i32) -> Self {
        Self::new(x, y, 0, 0)
    }

    fn add(&self, other: &Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
            w: self.w + other.w,
        }
    }

    fn max(&mut self, pos: &Self) {
        self.x = pos.x.max(self.x);
        self.y = pos.y.max(self.y);
        self.z = pos.z.max(self.z);
        self.w = pos.w.max(self.w);
    }

    fn min(&mut self, pos: &Self) {
        self.x = pos.x.min(self.x);
        self.y = pos.y.min(self.y);
        self.z = pos.z.min(self.z);
        self.w = pos.w.min(self.w);
    }

    fn next_for_neighbor_iter(&mut self) -> bool {
        match (self.x, self.y, self.z, self.w) {
            (0, 0, 0, 0) => self.set(-1, -1, -1, -1), // start at -1 ^ 3
            (-1, 0, 0, 0) => self.set(1, 0, 0, 0),    // skip zero
            (1, 1, 1, 1) => return false,             // last element
            (1, 1, 1, w) => self.set(-1, -1, -1, w + 1),
            (1, 1, z, w) => self.set(-1, -1, z + 1, w),
            (1, y, z, w) => self.set(-1, y + 1, z, w),
            (x, y, z, w) => self.set(x + 1, y, z, w),
        }

        true
    }

    fn next_for_space_iter(&self, min: &Self, max: &Self) -> Option<Box<Self>> {
        if self == max {
            None
        } else {
            let n = if self.x == max.x && self.y == max.y && self.z == max.z {
                Self::new(min.x, min.y, min.z, self.w + 1)
            } else if self.x == max.x && self.y == max.y {
                Self::new(min.x, min.y, self.z + 1, self.w)
            } else if self.x == max.x {
                Self::new(min.x, self.y + 1, self.z, self.w)
            } else {
                Self::new(self.x + 1, self.y, self.z, self.w)
            };
            Some(Box::new(n))
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day17::p1;

    #[test]
    fn test() {
        let data = aocdata::load("data/day17ex1.txt");
        let count = p1(&data);

        assert_eq!(count, 112);
    }
}
