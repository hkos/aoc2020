use crate::aocdata;

const SUBJECT_NUMBER: usize = 7;
const MOD: usize = 20201227;

pub fn part1() {
    let data = aocdata::load("data/day25.txt");

    let a = usize::from_str_radix(&data[0], 10).expect("bad num");
    let b = usize::from_str_radix(&data[1], 10).expect("bad num");

    let ls_a = find_loop_size(a);
    assert_eq!(ls_a, 250288);

    let ls_b = find_loop_size(b);
    assert_eq!(ls_b, 14519824);

    let enc1 = transform(a, ls_b);
    let enc2 = transform(b, ls_a);
    assert_eq!(enc1, enc2);

    println!("day25 part1: {}", enc1);
}

const fn step(n: usize, subj: usize, m: usize) -> usize {
    (n * subj) % m
}

fn transform(subj: usize, loop_size: usize) -> usize {
    let mut res = 1;
    for _ in 0..loop_size {
        res = step(res, subj, MOD);
    }
    res
}

fn find_loop_size(public: usize) -> usize {
    let mut i = 0;
    let mut val = 1;

    while val != public {
        val = step(val, SUBJECT_NUMBER, MOD);
        i += 1;
    }

    i
}

#[cfg(test)]
mod tests {
    use crate::day25::{find_loop_size, transform};

    #[test]
    fn test() {
        const CARD: usize = 5764801;
        const DOOR: usize = 17807724;

        let card_prv = find_loop_size(CARD);
        assert_eq!(card_prv, 8);

        let door_prv = find_loop_size(DOOR);
        assert_eq!(door_prv, 11);

        let enc1 = transform(DOOR, card_prv);
        let enc2 = transform(CARD, door_prv);

        assert_eq!(enc1, 14897079);
        assert_eq!(enc2, 14897079);
    }
}
