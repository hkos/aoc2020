use crate::aocdata;

use std::cmp::Ordering;
use std::collections::VecDeque;

pub fn part1() {
    let data = aocdata::load("data/day09p1.txt");
    let num = check(&aocdata::as_u64(&data), 25).expect("no result?!");
    assert_eq!(num, 393911906);

    println!("day09 part1: {}", num);
}

pub fn part2() {
    let data = aocdata::as_u64(&aocdata::load("data/day09p1.txt"));
    let num = check(&data, 25).expect("no result?!");
    assert_eq!(num, 393911906);

    let set = find_set(&data, num).unwrap();

    let weak = set_to_weakness(&set);
    assert_eq!(weak, 59341885);

    println!("day09 part2: {}", weak);
}

fn find_set(data: &[u64], sum: u64) -> Option<Vec<u64>> {
    'outer: for i in 0..data.len() - 1 {
        for end in i + 1..data.len() {
            match sum.cmp(&data[i..end].iter().sum()) {
                Ordering::Equal => {
                    return Some(data[i..end].to_vec());
                }
                Ordering::Less => {
                    continue 'outer;
                }
                _ => {}
            }
        }
    }

    None
}

fn set_to_weakness(set: &[u64]) -> u64 {
    let min = set.iter().min().unwrap();
    let max = set.iter().max().unwrap();
    min + max
}

fn is_pair_sum(pool: &VecDeque<u64>, check: u64) -> bool {
    for i in 0..pool.len() {
        for j in i + 1..pool.len() {
            if pool[i] + pool[j] == check {
                return true;
            }
        }
    }

    false
}

fn check(data: &[u64], preamble: usize) -> Option<u64> {
    let mut data = data.iter();

    let mut pool: VecDeque<u64> = VecDeque::new();
    for _ in 0..preamble {
        pool.push_back(*data.next().expect("no number"));
    }

    for &next in data {
        if !is_pair_sum(&pool, next) {
            return Some(next);
        }
        pool.push_back(next);
        let _ = pool.pop_front();
    }
    None
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day09::{check, find_set, set_to_weakness};

    #[test]
    fn test() {
        let data = aocdata::as_u64(&aocdata::load("data/day09ex1.txt"));
        let num = check(&data, 5).expect("no result?!");
        assert_eq!(num, 127);

        let set = find_set(&data, num).unwrap();
        assert_eq!(set, vec![15, 25, 47, 40]);

        let weak = set_to_weakness(&set);
        assert_eq!(weak, 62);
    }
}
