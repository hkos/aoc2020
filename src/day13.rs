use crate::aocdata;

use std::cmp::Ordering;
use std::collections::BTreeMap;

pub fn part1() {
    let data = aocdata::load("data/day13.txt");
    let res = p1(&data);

    assert_eq!(res, 3997);

    println!("day13 part1: {}", res);
}

pub fn part2() {
    let data = aocdata::load("data/day13.txt");
    let res = p2(&data);

    println!("day13 part2: {}", res);
    assert_eq!(res, 500033211739354);
}

struct Notes {
    timestamp: u64,
    busses: Vec<String>,
}

impl Notes {
    fn read(data: &[String]) -> Self {
        let timestamp: u64 = u64::from_str_radix(&data[0], 10).expect("bad number");
        let busses: Vec<String> = data[1].split(',').map(|b| b.to_owned()).collect();
        Self { timestamp, busses }
    }

    fn bus_lines(&self) -> Vec<u64> {
        self.busses
            .iter()
            .map(|b| u64::from_str_radix(b, 10))
            .flat_map(|b| b.ok())
            .collect()
    }

    // busline -> offset
    fn bus_lines2(&self) -> BTreeMap<usize, usize> {
        self.busses
            .iter()
            .enumerate()
            .filter(|(_, s)| s != &"x")
            .map(|(i, b)| (usize::from_str_radix(b, 10).expect("bad num"), i))
            .collect()
    }
}

fn next_bus(timestamp: u64, line: u64) -> u64 {
    let i = timestamp / line;

    if i % line == 0 {
        timestamp
    } else {
        (i + 1) * line
    }
}

// (line, timestamp)
fn next_busses(timestamp: u64, lines: &[u64]) -> Vec<(u64, u64)> {
    lines
        .iter()
        .map(|&line| (line, next_bus(timestamp, line)))
        .collect()
}

fn soonest(next: &[(u64, u64)]) -> (u64, u64) {
    next.iter().min_by_key(|(_, t)| t).cloned().unwrap()
}

fn p1(data: &[String]) -> u64 {
    let notes = Notes::read(data);

    let lines = notes.bus_lines();
    let next = next_busses(notes.timestamp, &lines);
    let soonest = soonest(&next);

    (soonest.1 - notes.timestamp) * soonest.0
}

// merge_line(stride, base, next_line) => (step, offset).
//
// the resulting tuple gives the minimal step-size and the first offset for
// merging both conditions
fn merge_line(step1: usize, off1: usize, step2: usize, off2: usize) -> (usize, usize) {
    let mut val1 = off1;
    let mut val2 = off2;

    loop {
        match usize::cmp(&val1, &val2) {
            Ordering::Less => {
                if (val2 - val1) % step1 == 0 {
                    val1 = val2;
                } else {
                    let n = (val2 - val1) / step1 + 1;
                    val1 += n * step1;
                }
            }
            Ordering::Greater => {
                if (val1 - val2) % step2 == 0 {
                    val2 = val1;
                } else {
                    let n = (val1 - val2) / step2 + 1;
                    val2 += n * step2;
                }
            }
            Ordering::Equal => {
                // found intersection for these two conditions at
                // offset val1==val2

                // -> "merged" minimal step size is lcm of step1 and step2
                let lcm = num::integer::lcm(step1, step2);

                return (lcm, val1);
            }
        }
    }
}

fn p2(data: &[String]) -> usize {
    let notes = Notes::read(data);

    let lines = notes.bus_lines2();

    let mut iter = lines.iter().map(|(&a, &b)| {
        let b = b % a;
        (a, a - b)
    });

    let (line1, offset1) = iter.next().unwrap();

    // expand these for each bus line
    let mut stride = line1;
    let mut base = offset1;

    for next_line in iter {
        let merged = merge_line(stride, base, next_line.0, next_line.1);
        stride = merged.0;
        base = merged.1;
    }

    base
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day13::{p1, p2};

    #[test]
    fn test1() {
        let data = aocdata::load("data/day13ex1.txt");
        let res = p1(&data);

        assert_eq!(res, 295);
    }

    #[test]
    fn test2() {
        let res = p2(&["0".to_string(), "67,7,59,61".to_string()]);
        assert_eq!(res, 754018);

        let res = p2(&["0".to_string(), "67,x,7,59,61".to_string()]);
        assert_eq!(res, 779210);

        let res = p2(&["0".to_string(), "67,7,x,59,61".to_string()]);
        assert_eq!(res, 1261476);

        let res = p2(&["0".to_string(), "1789,37,47,1889".to_string()]);
        assert_eq!(res, 1202161486);
    }
}
