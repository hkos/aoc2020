use crate::aocdata;

use std::collections::{HashSet, VecDeque};
use std::slice::Iter;

pub fn part1() {
    let data = aocdata::load("data/day22.txt");
    let mut game: Combat = Game::from(&data);

    game.play();

    let score = game
        .winning_deck()
        .expect("can't score this game state")
        .score();

    assert_eq!(score, 31308);

    println!("day22 part1: {}", score);
}

pub fn part2() {
    let data = aocdata::load("data/day22.txt");
    let mut game: RecursiveCombat = Game::from(&data);

    game.play();

    let score = game
        .winning_deck()
        .expect("can't score this game state")
        .score();

    assert_eq!(score, 33647);

    println!("day22 part2: {}", score);
}

trait Game: Sized {
    fn new(p1: Deck, p2: Deck) -> Self;

    fn from(data: &[String]) -> Self {
        let mut iter = data.iter();

        assert_eq!(iter.next().expect("input file bad"), "Player 1:");
        let p1 = Deck::from(&mut iter);

        assert_eq!(iter.next().expect("input file bad"), "Player 2:");
        let p2 = Deck::from(&mut iter);

        Self::new(p1, p2)
    }

    fn done(&self) -> bool;
    fn winning_deck(self) -> Option<Deck>;

    fn step(&mut self);

    fn play(&mut self) {
        while !self.done() {
            self.step();
        }
    }
}

#[derive(Debug)]
struct RecursiveCombat {
    p1: Option<Deck>,
    p2: Option<Deck>,
    round_prev_state: HashSet<(Deck, Deck)>,
    winner: Option<Deck>,
}

impl Game for RecursiveCombat {
    fn new(p1: Deck, p2: Deck) -> Self {
        RecursiveCombat {
            p1: Some(p1),
            p2: Some(p2),
            round_prev_state: HashSet::new(),
            winner: None,
        }
    }

    fn done(&self) -> bool {
        self.winner.is_some()
    }

    fn winning_deck(self) -> Option<Deck> {
        self.winner
    }

    fn step(&mut self) {
        if self.winner.is_some() {
            panic!("step() may not be called on a finished game!");
        }

        if self.p1.as_ref().unwrap().cards.is_empty() {
            self.winner = self.p2.take();
            return;
        }

        if self.p2.as_ref().unwrap().cards.is_empty() {
            self.winner = self.p1.take();
            return;
        }

        // if there was a previous round in this game that had exactly the
        // same cards in the same order in the same players' decks, the
        // game instantly ends in a win for player 1
        if self.round_prev_state.contains(&(
            self.p1.as_ref().unwrap().clone(),
            self.p2.as_ref().unwrap().clone(),
        )) {
            self.winner = self.p1.take();
            return;
        }

        // store game state for loop-detection
        let old1 = self.p1.as_ref().unwrap().clone();
        let old2 = self.p2.as_ref().unwrap().clone();
        self.round_prev_state.insert((old1, old2));

        // ---

        let draw_p1 = self.p1.as_mut().unwrap().draw();
        let draw_p2 = self.p2.as_mut().unwrap().draw();

        if self.p1.as_ref().unwrap().cards.len() >= draw_p1
            && self.p2.as_ref().unwrap().cards.len() >= draw_p2
        {
            let sub1: VecDeque<_> = self
                .p1
                .as_ref()
                .unwrap()
                .cards
                .iter()
                .take(draw_p1)
                .cloned()
                .collect();
            let sub2: VecDeque<_> = self
                .p2
                .as_ref()
                .unwrap()
                .cards
                .iter()
                .take(draw_p2)
                .cloned()
                .collect();

            let mut sub = RecursiveCombat::new(Deck { cards: sub1 }, Deck { cards: sub2 });
            sub.play();

            if sub.p1.is_none() {
                // p1 won subgame
                self.p1.as_mut().unwrap().push(draw_p1);
                self.p1.as_mut().unwrap().push(draw_p2);
            } else {
                self.p2.as_mut().unwrap().push(draw_p2);
                self.p2.as_mut().unwrap().push(draw_p1);
            }
        } else {
            // at least one player must not have enough cards left in
            // their deck to recurse; the winner of the round is the
            // player with the higher-value card

            if draw_p1 > draw_p2 {
                self.p1.as_mut().unwrap().push(draw_p1);
                self.p1.as_mut().unwrap().push(draw_p2);
            } else {
                self.p2.as_mut().unwrap().push(draw_p2);
                self.p2.as_mut().unwrap().push(draw_p1);
            }
        }
    }
}

#[derive(Debug)]
struct Combat {
    p1: Deck,
    p2: Deck,
}

impl Game for Combat {
    fn new(p1: Deck, p2: Deck) -> Self {
        Combat { p1, p2 }
    }
    fn done(&self) -> bool {
        self.p1.cards.is_empty() || self.p2.cards.is_empty()
    }

    fn winning_deck(self) -> Option<Deck> {
        match (self.p1.cards.is_empty(), self.p2.cards.is_empty()) {
            (true, false) => Some(self.p2),
            (false, true) => Some(self.p1),
            _ => None,
        }
    }

    fn step(&mut self) {
        let draw_p1 = self.p1.draw();
        let draw_p2 = self.p2.draw();

        if draw_p1 > draw_p2 {
            self.p1.cards.push_back(draw_p1);
            self.p1.cards.push_back(draw_p2);
        } else {
            self.p2.cards.push_back(draw_p2);
            self.p2.cards.push_back(draw_p1);
        }
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
struct Deck {
    cards: VecDeque<usize>,
}

impl Deck {
    fn from(iter: &mut Iter<String>) -> Self {
        let cards = iter
            .take_while(|l| !l.is_empty())
            .map(|l| usize::from_str_radix(l, 10).expect("bad card"))
            .collect();

        Self { cards }
    }

    fn draw(&mut self) -> usize {
        self.cards.pop_front().expect("no card left in deck")
    }

    fn push(&mut self, card: usize) {
        self.cards.push_back(card);
    }

    fn score(&self) -> usize {
        self.cards
            .iter()
            .rev()
            .enumerate()
            .map(|(i, c)| (i + 1) * c)
            .sum()
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day22::{Combat, Game, RecursiveCombat};

    #[test]
    fn test() {
        let data = aocdata::load("data/day22ex1.txt");
        let mut game: Combat = Game::from(&data);

        game.play();
        let score = game.winning_deck().expect("no winner?").score();

        assert_eq!(score, 306);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day22ex1.txt");
        let mut rc: RecursiveCombat = Game::from(&data);

        rc.play();
        let score = rc.winning_deck().expect("no winner?").score();

        assert_eq!(score, 291);
    }

    #[test]
    fn test3() {
        let data = aocdata::load("data/day22ex2.txt");
        let mut rc: RecursiveCombat = Game::from(&data);

        rc.play(); // infinite game!

        let score = rc.winning_deck().expect("no winner?").score();
        assert_eq!(score, 105); // ?
    }
}
