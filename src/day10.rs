use crate::aocdata;

pub fn part1() {
    let data = aocdata::as_u64(&aocdata::load("data/day10p1.txt"));

    let num = p1(&data);
    assert_eq!(num, 2450);

    println!("day10 part1: {}", num);
}

pub fn part2() {
    let data = aocdata::as_u64(&aocdata::load("data/day10p1.txt"));

    let p2 = p2(&data);
    assert_eq!(p2, 32396521357312);

    println!("day10 part2: {}", p2);
}

fn diffs(data: &[u64]) -> Vec<u64> {
    let mut data = data.to_vec();

    // add outlet
    data.push(0);

    // sort adapters
    data.sort_unstable();
    let max = data[data.len() - 1];

    // add device
    data.push(max + 3);

    data.iter()
        .zip(data[1..].iter())
        .map(|(a, b)| b - a)
        .collect()
}

fn runs_of_one(diffs: &[u64]) -> Vec<usize> {
    let mut res = vec![];

    let mut run = vec![];
    for &d in diffs {
        if d == 1 {
            run.push(d);
        } else if d == 3 {
            res.push(run);
            run = vec![];
        }
    }

    res.iter().map(|run| run.len()).filter(|&l| l > 0).collect()
}

fn combinations_in_run(len: usize) -> usize {
    match len {
        0 => 1,
        1 => 1,

        // 1 1 -> 11 2
        2 => 2,

        // 1 1 1 -> 111 21 12 3
        3 => 4,

        // 1 1 1 1 -> 1111 211 121 112 22 13 31
        4 => 7,

        // 1 1 1 1 1 ->
        // 11111
        // 1112 1121 1211 2111
        // 113 131 311
        // 212 122 221
        // 23 32
        5 => 13,

        _ => unimplemented!(),
    }
}

fn p2(data: &[u64]) -> usize {
    let diffs = diffs(&data);
    let runs = runs_of_one(&diffs);

    let mut res = 1;
    for r in runs {
        res *= combinations_in_run(r);
    }
    res
}

fn count(data: &[u64], c: u64) -> usize {
    data.iter().filter(|&&a| a == c).count()
}

fn p1(data: &[u64]) -> usize {
    let diffs = diffs(data);
    count(&diffs, 1) * count(&diffs, 3)
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day10::{p1, p2};

    #[test]
    fn test() {
        let data = aocdata::as_u64(&aocdata::load("data/day10ex1.txt"));
        let num = p1(&data);
        assert_eq!(num, 7 * 5);
    }

    #[test]
    fn test2() {
        let data = aocdata::as_u64(&aocdata::load("data/day10ex2.txt"));
        let num = p1(&data);
        assert_eq!(num, 22 * 10);
    }

    #[test]
    fn test3() {
        let data = aocdata::as_u64(&aocdata::load("data/day10ex1.txt"));
        let p2 = p2(&data);

        assert_eq!(p2, 8);
    }

    #[test]
    fn test4() {
        let data = aocdata::as_u64(&aocdata::load("data/day10ex2.txt"));
        let p2 = p2(&data);

        assert_eq!(p2, 19208);
    }
}
