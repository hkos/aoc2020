use crate::aocdata;

use regex::Regex;
use std::collections::{HashMap, HashSet};

pub fn part1() {
    let data = aocdata::load("data/day20.txt");
    let images = Images::from(&data);
    let puz = Puzzle::solve(images);

    let mut res = 1;
    let corners = puz.corner_ids();
    corners.iter().for_each(|c| res *= c);

    assert_eq!(res, 16937516456219);

    println!("day20 part1: {}", res);
}

pub fn part2() {
    let data = aocdata::load("data/day20.txt");
    let images = Images::from(&data);
    let puz = Puzzle::solve(images);
    let bitmap = puz.get_borderless();

    let roughness = roughness(&bitmap);
    assert_eq!(roughness, 1858);

    println!("day20 part2: {}", roughness);
}

fn roughness(bitmap: &Bitmap) -> usize {
    find_and_mask_monsters(bitmap)
        .pixel
        .iter()
        .map(|l| l.iter().filter(|&&b| b).count())
        .sum()
}

fn find_and_mask_monsters(bitmap: &Bitmap) -> Bitmap {
    let (oriented, locs) = find_monsters(bitmap);
    let mut masked = oriented;
    for loc in locs {
        masked = masked.mask_bitmap(&SEA_MONSTER, loc.0, loc.1);
    }

    masked
}

fn find_monsters(bitmap: &Bitmap) -> (Bitmap, Vec<(usize, usize)>) {
    for rot in 0..4 {
        for flip in &[false, true] {
            let mut finds = Vec::new();

            let bitmap = bitmap.rot_flip(rot, *flip);

            let (sx, sy) = bitmap.size();
            for x in 0..(sx - SEA_MONSTER.size().0) {
                for y in 0..(sy - SEA_MONSTER.size().1) {
                    if bitmap.match_bitmap(&SEA_MONSTER, x, y) {
                        finds.push((x, y));
                    }
                }
            }
            if !finds.is_empty() {
                return (bitmap, finds);
            }
        }
    }

    panic!("no monsters found")
}

lazy_static! {
    static ref SEA_MONSTER: Bitmap = Bitmap::from(&[
        "..................#.".to_string(),
        "#....##....##....###".to_string(),
        ".#..#..#..#..#..#...".to_string(),
    ]);
}

#[derive(Debug)]
struct Puzzle {
    // the puzzle is square
    size: usize,

    // x + size*y
    placed: Vec<Placed>,

    images: HashMap<usize, Image>,
}

impl Puzzle {
    fn get_borderless(&self) -> Bitmap {
        let mut pixel = Vec::new();

        for tiles_y in 0..self.size {
            let tiles: Vec<_> = (0..self.size)
                .map(|tiles_x| self.get_rot_flip_strip(tiles_x, tiles_y))
                .collect();

            for tile_line in 0..tiles[0].size().1 {
                let full_line: Vec<bool> = tiles
                    .iter()
                    .flat_map(|bm| bm.pixel[tile_line].iter())
                    .cloned()
                    .collect();
                pixel.push(full_line);
            }
        }

        Bitmap { pixel }
    }

    fn corner_ids(&self) -> [usize; 4] {
        [
            self.get(0, 0).unwrap().id,
            self.get(self.size - 1, 0).unwrap().id,
            self.get(0, self.size - 1).unwrap().id,
            self.get(self.size - 1, self.size - 1).unwrap().id,
        ]
    }

    fn solve(images: Images) -> Self {
        let img_set = images.images.iter().map(|i| i.id).collect();

        let p = Puzzle::new(images.square_size(), images.images);

        let (solution, found) = p.recurse(img_set);
        if !found {
            panic!("no solution found");
        } else {
            solution
        }
    }

    fn next_pos(&self) -> (usize, usize) {
        let next = self.placed.len();
        (next % self.size, next / self.size)
    }

    fn new(size: usize, images: Vec<Image>) -> Self {
        let images = images.into_iter().map(|i| (i.id, i)).collect();
        Self {
            size,
            images,
            placed: Vec::with_capacity(size * size),
        }
    }

    fn check(e1: &str, e2: &str) -> bool {
        !e1.chars().zip(e2.chars().rev()).any(|(c1, c2)| c1 != c2)
    }

    // only need to check leftward and upwards
    // (as we're placing top to bottom, left to right)
    fn fits(&self, x: usize, y: usize, place: &Placed) -> bool {
        // check "my left" against "prev right"
        if x > 0 {
            let my_left = &place.edges[3];
            let prev_right = &self.get(x - 1, y).unwrap().edges[1];

            if !Self::check(my_left, prev_right) {
                return false;
            }
        }

        // check "my top against "upper bottom"
        if y > 0 {
            let my_top = &place.edges[0];
            let upper_bottom = &self.get(x, y - 1).unwrap().edges[2];

            if !Self::check(my_top, upper_bottom) {
                return false;
            }
        }

        true
    }

    fn try_append(&mut self, place: Placed) -> bool {
        let (nx, ny) = self.next_pos();

        if self.fits(nx, ny, &place) {
            self.placed.push(place);
            true
        } else {
            false
        }
    }

    fn recurse(self, images: HashSet<usize>) -> (Self, bool) {
        let mut puz = self;

        if images.is_empty() {
            // solution found
            return (puz, true);
        }

        for i in &images {
            let mut remain = images.clone();
            remain.remove(&i);

            let img = puz.images.get(i).unwrap().clone();

            // try placing 'img' in all variant
            for flip in &[false, true] {
                for rot in 0..=3 {
                    let pl = Placed::rot_flip(&img, *flip, rot);

                    // variant
                    let ok = puz.try_append(pl);

                    if ok {
                        // recurse for all successful variants
                        let (mut s, found) = puz.recurse(remain.clone());
                        if found {
                            return (s, true);
                        }

                        s.placed.pop();
                        puz = s;
                    }
                }
            }
        }

        // no solution found in this branch
        (puz, false)
    }

    fn get(&self, tiles_x: usize, tiles_y: usize) -> Option<&Placed> {
        self.placed.get(tiles_x + self.size * tiles_y)
    }

    fn get_rot_flip_strip(&self, tiles_x: usize, tiles_y: usize) -> Bitmap {
        let pl = self.get(tiles_x, tiles_y).expect("tile missing");
        let img = self.images.get(&pl.id).unwrap();

        let mut bm = img.bitmap.rot_flip(pl.rot, pl.flip);

        bm = bm.strip_edge();

        bm
    }
}

#[derive(Clone, Debug)]
struct Placed {
    id: usize,
    rot: usize,
    flip: bool,
    edges: [String; 4],
}

impl Placed {
    /// flip: flip horizontally, first
    /// rot: rotation 0..3
    fn rot_flip(image: &Image, flip: bool, rot: usize) -> Self {
        let n = rot + if flip { 4 } else { 0 };
        let edges = image.edge_cache.get(n).unwrap().clone();

        Self {
            edges,
            rot,
            flip,
            id: image.id,
        }
    }
}

#[derive(Debug)]
struct Images {
    images: Vec<Image>,
}

impl Images {
    fn square_size(&self) -> usize {
        let sqrt = (self.images.len() as f64).sqrt() as usize;
        assert_eq!(sqrt * sqrt, self.images.len());
        sqrt
    }

    fn from(data: &[String]) -> Self {
        let mut lines = data.iter();
        let mut images = vec![];

        while let Some(line) = lines.next() {
            lazy_static! {
                static ref TILE_RE: Regex = Regex::new(r"^Tile (\d+):$").unwrap();
            }

            let cap = TILE_RE.captures(&line).unwrap();
            let n: &str = cap.get(1).unwrap().as_str();
            let id = usize::from_str_radix(n, 10).expect("bad num");

            let mut tile_str = Vec::new();

            loop {
                let line = lines.next();
                if line.is_none() || line.unwrap().is_empty() {
                    break;
                }

                tile_str.push(line.unwrap().clone());
            }
            let bitmap = Bitmap::from(&tile_str);
            let img = Image::new(id, bitmap);
            images.push(img);
        }

        Self { images }
    }
}

#[derive(Clone, Debug)]
struct Bitmap {
    pixel: Vec<Vec<bool>>,
}

impl Bitmap {
    fn from(lines: &[String]) -> Self {
        let mut pixel = Vec::new();

        for line in lines {
            let px: Vec<bool> = line
                .chars()
                .into_iter()
                .map(|c| match c {
                    '#' => true,
                    '.' => false,
                    _ => panic!("unexpected char"),
                })
                .collect();
            pixel.push(px);
        }

        Self { pixel }
    }

    // all "true" pixels in 'other' must match 'true' pixels in self
    fn match_bitmap(&self, other: &Bitmap, x_offset: usize, y_offset: usize) -> bool {
        let (sx, sy) = other.size();

        for y in 0..sy {
            for x in 0..sx {
                if other.get(x, y) && !self.get(x + x_offset, y + y_offset) {
                    return false;
                }
            }
        }

        true
    }

    // all "true" pixels in 'other' will disable pixels in self
    fn mask_bitmap(&self, other: &Bitmap, x_offset: usize, y_offset: usize) -> Bitmap {
        let mut masked = self.pixel.clone();

        let (sx, sy) = other.size();

        for y in 0..sy {
            for x in 0..sx {
                if other.get(x, y) {
                    masked[y + y_offset][x + x_offset] = false;
                }
            }
        }

        Self { pixel: masked }
    }

    fn size(&self) -> (usize, usize) {
        (self.pixel[0].len(), self.pixel.len())
    }

    fn get(&self, x: usize, y: usize) -> bool {
        self.pixel[y][x]
    }

    fn strip_edge(&self) -> Bitmap {
        let (sx, sy) = self.size();
        let mut pixel = Vec::new();
        for y in 1..(sy - 1) {
            let line: Vec<_> = self.pixel[y].iter().skip(1).take(sx - 2).cloned().collect();
            pixel.push(line);
        }
        Bitmap { pixel }
    }

    fn rot_flip(&self, rot: usize, flip: bool) -> Bitmap {
        let mut bm = self.clone();
        if flip {
            bm = bm.flip();
        }
        bm.rot(rot)
    }

    // rot 90deg clockwise
    fn rot1(&self) -> Bitmap {
        let (sx, sy) = self.size();

        let mut pixel = Vec::new();

        for y_new in 0..sx {
            let l: Vec<_> = (0..sy)
                .map(|x_new| self.get(y_new, sy - x_new - 1))
                .collect();
            pixel.push(l);
        }

        Bitmap { pixel }
    }

    fn rot(&self, rot: usize) -> Bitmap {
        let rot = rot % 4;

        let mut res = self.clone();
        for _ in 0..rot {
            res = res.rot1();
        }
        res
    }

    fn flip(&self) -> Bitmap {
        let pixel: Vec<Vec<bool>> = self
            .pixel
            .iter()
            .map(|l| l.iter().rev().cloned().collect())
            .collect();

        Bitmap { pixel }
    }

    fn bool2char(b: bool) -> char {
        match b {
            true => '#',
            false => '.',
        }
    }

    // TOP, RIGHT, BOTTOM, LEFT (all clockwise)
    fn edges(&self) -> [String; 4] {
        let (sx, sy) = self.size();

        let left: String = (0..sy)
            .rev()
            .map(|y| self.get(0, y))
            .map(Self::bool2char)
            .collect();
        let right: String = (0..sy)
            .map(|y| self.get(sx - 1, y))
            .map(Self::bool2char)
            .collect();

        let top: String = (0..sx)
            .map(|x| self.get(x, 0))
            .map(Self::bool2char)
            .collect();
        let bottom: String = (0..sx)
            .rev()
            .map(|x| self.get(x, sy - 1))
            .map(Self::bool2char)
            .collect();

        [top, right, bottom, left]
    }

    #[allow(dead_code)]
    fn print(&self) {
        for line in &self.pixel {
            let l: String = line.iter().map(|px| Self::bool2char(*px)).collect();
            println!("{}", l);
        }
    }
}

#[derive(Clone, Debug)]
struct Image {
    bitmap: Bitmap,
    id: usize,

    edge_cache: Vec<[String; 4]>,
}

impl Image {
    fn new(id: usize, bitmap: Bitmap) -> Self {
        let mut i = Self {
            id,
            bitmap,
            edge_cache: Vec::with_capacity(8),
        };

        // cache rotations
        i.init();

        i
    }

    fn init(&mut self) {
        for flip in &[false, true] {
            for rot in 0..=3 {
                let bm = self.bitmap.rot_flip(rot, *flip);
                self.edge_cache.push(bm.edges());
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day20::{find_monsters, roughness, Bitmap, Images, Puzzle};

    #[test]
    fn test() {
        let data = aocdata::load("data/day20ex1.txt");

        let images = Images::from(&data);

        let count = images.images.len();
        assert_eq!(count, 9);

        let puz = Puzzle::solve(images);
        // println!("{:?}", puz);

        let mut res = 1;
        let corners = puz.corner_ids();
        corners.iter().for_each(|c| res *= c);

        assert_eq!(res, 20899048083289);

        let bitmap = puz.get_borderless();
        bitmap.print();

        let (_bm, loc) = find_monsters(&bitmap);
        assert_eq!(2, loc.len());

        let roughness = roughness(&bitmap);
        assert_eq!(roughness, 273);
    }

    #[test]
    fn test_bm() {
        let bm = Bitmap {
            pixel: vec![vec![true, false, true], vec![false, false, true]],
        };

        let bm_flip = bm.flip();
        assert_eq!(
            bm_flip.pixel,
            vec![vec![true, false, true], vec![true, false, false]]
        );

        let bm_rot1 = bm.rot1();
        assert_eq!(
            bm_rot1.pixel,
            vec![vec![false, true], vec![false, false], vec![true, true]]
        );

        let bm_rot3 = bm.rot(3);
        assert_eq!(
            bm_rot3.pixel,
            vec![vec![true, true], vec![false, false], vec![true, false]]
        );
    }
}
