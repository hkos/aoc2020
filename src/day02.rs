use crate::aocdata;

pub fn part1() {
    let pw = as_pw(&aocdata::load("data/day02p1.txt"));
    println!("day02 part1: {}", num_good_v1(&pw));
}

pub fn part2() {
    let pw = as_pw(&aocdata::load("data/day02p1.txt"));
    println!("day02 part2: {}", num_good_v2(&pw));
}

#[derive(Debug)]
struct PasswordLine {
    num1: usize,
    num2: usize,
    ch: char,
    pw: String,
}

impl PasswordLine {
    fn from(line: &str) -> Self {
        // split line into (hopefully three) parts
        let parts: Vec<_> = line.split(' ').collect();
        assert_eq!(parts.len(), 3);

        // process first part
        let nums: Vec<_> = parts[0].split('-').collect();
        assert_eq!(nums.len(), 2);
        let num1 = usize::from_str_radix(nums[0], 10).expect("can't parse num1");
        let num2 = usize::from_str_radix(nums[1], 10).expect("can't parse num2");

        // process second part
        assert_eq!(parts[1].len(), 2);
        let ch = parts[1]
            .chars()
            .into_iter()
            .next()
            .expect("can't parse pw char");

        // third part
        let pw = parts[2].to_owned();

        Self { num1, num2, ch, pw }
    }

    /// this is the check for part1
    /// (which turns out to be the wrong checking policy)
    fn check1(&self) -> bool {
        let count = self
            .pw
            .chars()
            .into_iter()
            .filter(|&c| c == self.ch)
            .count();

        count >= self.num1 && count <= self.num2
    }

    /// this is the check for part2
    pub fn check2(&self) -> bool {
        let chars: Vec<_> = self.pw.chars().collect();
        let c1 = chars[self.num1 - 1];
        let c2 = chars[self.num2 - 1];

        (c1 == self.ch && c2 != self.ch) || (c1 != self.ch && c2 == self.ch)
    }
}

fn as_pw(data: &[String]) -> Vec<PasswordLine> {
    data.iter().map(|line| PasswordLine::from(line)).collect()
}

fn num_good_v1(pw: &[PasswordLine]) -> usize {
    pw.iter().filter(|pw| pw.check1()).count()
}

fn num_good_v2(pw: &[PasswordLine]) -> usize {
    pw.iter().filter(|pw| pw.check2()).count()
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day02::{as_pw, num_good_v1, num_good_v2};

    #[test]
    fn test() {
        let data = aocdata::load("data/day02ex1.txt");
        let pw = as_pw(&data);

        assert_eq!(num_good_v1(&pw), 2);
        assert_eq!(num_good_v2(&pw), 1);
    }
}
