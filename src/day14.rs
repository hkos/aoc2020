use crate::aocdata;

use regex::Regex;
use std::collections::HashMap;

pub fn part1() {
    let data = aocdata::load("data/day14.txt");
    let prog = Program::from(&data);

    let cmp = DecoderV1::run(&prog);
    let sum = sum_memory(&cmp.mem);

    assert_eq!(sum, 18630548206046);

    println!("day14 part1: {}", sum);
}

pub fn part2() {
    let data = aocdata::load("data/day14.txt");
    let prog = Program::from(&data);

    let cmp = DecoderV2::run(&prog);
    let sum = sum_memory(&cmp.mem);

    assert_eq!(sum, 4254673508445);

    println!("day14 part2: {}", sum);
}

fn sum_memory(mem: &HashMap<usize, usize>) -> usize {
    mem.iter().map(|(_, &val)| val).sum()
}

fn apply_mask(val: &str, mask: &str, mask_fn: fn(char, char) -> char) -> String {
    assert_eq!(val.len(), 36);
    assert_eq!(mask.len(), 36);

    val.chars()
        .zip(mask.chars())
        .map(|(c, m)| mask_fn(c, m))
        .collect()
}

fn mask_fn_v1(c: char, m: char) -> char {
    match (c, m) {
        (_, '1') => '1',
        (_, '0') => '0',
        (c, _) => c,
    }
}

fn mask_fn_v2(c: char, m: char) -> char {
    match (c, m) {
        (c, '0') => c,
        (_, '1') => '1',
        (_, 'X') => 'X',
        _ => panic!("illegal masking operation"),
    }
}

#[derive(Debug)]
struct DecoderV1 {
    mem: HashMap<usize, usize>,
}

impl DecoderV1 {
    fn run(program: &Program) -> Self {
        let mut c = DecoderV1 {
            mem: HashMap::new(),
        };
        let mut mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

        for cmd in &program.cmd {
            match cmd {
                Command::Mask(m) => {
                    mask = m;
                }
                Command::Mem(addr, val) => {
                    let val_bin = format!("{:036b}", val);

                    let masked = apply_mask(&val_bin, mask, mask_fn_v1);
                    assert_eq!(masked.len(), 36);

                    let u = usize::from_str_radix(&masked, 2).expect("unexpected number format");
                    c.mem.insert(*addr, u);
                }
            }
        }

        c
    }
}

#[derive(Debug)]
struct DecoderV2 {
    mem: HashMap<usize, usize>,
}

impl DecoderV2 {
    fn recurse(a: Vec<Vec<char>>, pos: &[usize]) -> Vec<String> {
        if pos.is_empty() {
            a.iter().map(|v| v.iter().collect()).collect()
        } else {
            let mut doubled = Vec::new();

            for n in a {
                let mut n0 = n.clone();
                n0[pos[0]] = '0';

                let mut n1 = n.clone();
                n1[pos[0]] = '1';

                doubled.push(n0);
                doubled.push(n1);
            }

            Self::recurse(doubled, &pos[1..])
        }
    }

    fn mask_addr(addr: usize, mask: &str) -> Vec<usize> {
        let addr_bin = format!("{:036b}", addr);
        let masked = apply_mask(&addr_bin, mask, mask_fn_v2);

        let xs: Vec<_> = masked
            .chars()
            .enumerate()
            .filter(|(_, c)| *c == 'X')
            .map(|(i, _)| i)
            .collect();

        Self::recurse(vec![masked.chars().collect()], &xs)
            .iter()
            .map(|a| usize::from_str_radix(a, 2).expect("bad number"))
            .collect()
    }

    fn run(program: &Program) -> Self {
        let mut c = DecoderV2 {
            mem: HashMap::new(),
        };
        let mut mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

        for cmd in &program.cmd {
            match cmd {
                Command::Mask(m) => mask = m,
                Command::Mem(addr, val) => {
                    let masked_addr: Vec<usize> = Self::mask_addr(*addr, mask);

                    for a in masked_addr {
                        c.mem.insert(a, *val);
                    }
                }
            }
        }

        c
    }
}

#[derive(Debug)]
struct Program {
    cmd: Vec<Command>,
}

impl Program {
    fn from(data: &[String]) -> Self {
        let mut cmd = Vec::new();

        for line in data {
            cmd.push(Command::from(line));
        }

        Program { cmd }
    }
}

#[derive(Debug)]
enum Command {
    Mask(String),
    Mem(usize, usize),
}

impl Command {
    fn from(line: &str) -> Self {
        lazy_static! {
            static ref MASK_RE: Regex = Regex::new(r#"^mask = ([X10]+)$"#).unwrap();
            static ref MEM_RE: Regex = Regex::new(r"^mem\[(\d+)\] = (\d+)$").unwrap();
        }

        if MASK_RE.is_match(line) {
            let cap = MASK_RE.captures(&line).unwrap();
            let mask: &str = cap.get(1).unwrap().as_str();

            Self::Mask(mask.to_string())
        } else if MEM_RE.is_match(&line) {
            let cap = MEM_RE.captures(&line).unwrap();
            let n: &str = cap.get(1).unwrap().as_str();
            let n = usize::from_str_radix(n, 10).expect("bad num");
            let val: &str = cap.get(2).unwrap().as_str();
            let val = usize::from_str_radix(val, 10).expect("bad num");

            Self::Mem(n, val)
        } else {
            panic!("unexpected line");
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day14::{sum_memory, DecoderV1, DecoderV2, Program};

    #[test]
    fn test() {
        let data = aocdata::load("data/day14ex1.txt");
        let prog = Program::from(&data);

        println!("{:#?}", prog);

        let cmp = DecoderV1::run(&prog);
        println!("cmp: {:?}", cmp);

        let sum = sum_memory(&cmp.mem);

        assert_eq!(165, sum);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day14ex2.txt");
        let prog = Program::from(&data);

        println!("{:#?}", prog);

        let cmp = DecoderV2::run(&prog);
        println!("cmp: {:?}", cmp);

        let sum = sum_memory(&cmp.mem);

        assert_eq!(208, sum);
    }
}
