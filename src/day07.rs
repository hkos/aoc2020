use crate::aocdata;

use regex::Regex;
use std::collections::{HashMap, HashSet};

pub fn part1() {
    let data = aocdata::load("data/day07p1.txt");
    let ruleset = RuleSet::from(&data);

    let container = ruleset.can_contain("shiny gold");
    assert_eq!(container.len(), 139);

    println!("day07 part1: {}", container.len());
}

pub fn part2() {
    let data = aocdata::load("data/day07p1.txt");
    let ruleset = RuleSet::from(&data);

    let num = ruleset.num_contained("shiny gold");
    assert_eq!(num, 58175);

    println!("day07 part2: {}", num);
}

struct RuleSet {
    rules: HashMap<String, Rule>,
}

impl RuleSet {
    fn from(data: &[String]) -> Self {
        let rules = data
            .iter()
            .map(|line| Rule::from(line))
            .map(|rule| (rule.container.clone(), rule))
            .collect();

        Self { rules }
    }

    fn can_contain(&self, bag: &str) -> HashSet<String> {
        let mut upstream: HashSet<String> = self
            .rules
            .values()
            .filter(|rule| rule.can_contain(bag))
            .map(|rule| rule.container.clone())
            .collect();

        let mut new = upstream.clone();

        loop {
            let mut recurse = HashSet::new();
            for bag1 in &new {
                self.can_contain(bag1).iter().for_each(|x| {
                    let _ = recurse.insert(x.clone());
                });
            }

            if recurse.is_subset(&upstream) {
                // no new upstream bag types found
                break;
            }

            // find more parents for newly found upstream bag types
            new = recurse.difference(&upstream).cloned().collect();
            recurse.iter().for_each(|r| {
                let _ = upstream.insert(r.clone());
            });
        }

        upstream
    }

    fn num_contained(&self, bag: &str) -> usize {
        let rule = self.rules.get(bag).expect("not found");

        if rule.contains.is_empty() {
            0
        } else {
            rule.contains
                .iter()
                .map(|(count, inner)| count * (1 + self.num_contained(inner)))
                .sum()
        }
    }
}

#[derive(Debug)]
struct Rule {
    container: String,
    contains: Vec<(usize, String)>,
}

impl Rule {
    fn from(line: &str) -> Rule {
        let parts: Vec<_> = line.split(" bags contain ").collect();
        assert_eq!(parts.len(), 2);

        let container = parts[0].to_string();

        let mut contains = vec![];

        if parts[1] != "no other bags." {
            lazy_static! {
                static ref RIGHT_RE: Regex = Regex::new(r"^(\d)+ ([a-z ]*) (bag|bags)").unwrap();
            }

            for s in parts[1].split(", ") {
                let cap = RIGHT_RE.captures(s).unwrap();

                let count = cap.get(1).unwrap().as_str();
                let bag_type = cap.get(2).unwrap().as_str();

                contains.push((
                    usize::from_str_radix(count, 10).expect("bad number"),
                    bag_type.to_string(),
                ));
            }
        }

        Rule {
            container,
            contains,
        }
    }

    fn can_contain(&self, bag: &str) -> bool {
        for (_, container) in &self.contains {
            if bag == container {
                return true;
            }
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day07::RuleSet;

    #[test]
    fn test() {
        let data = aocdata::load("data/day07ex1.txt");
        let ruleset = RuleSet::from(&data);

        let possible_parents = ruleset.can_contain("shiny gold");
        assert_eq!(possible_parents.len(), 4);
    }

    #[test]
    fn test2a() {
        let data = aocdata::load("data/day07ex1.txt");
        let ruleset = RuleSet::from(&data);
        assert_eq!(ruleset.num_contained("shiny gold"), 32);
    }

    #[test]
    fn test2b() {
        let data = aocdata::load("data/day07ex2.txt");
        let ruleset = RuleSet::from(&data);
        assert_eq!(ruleset.num_contained("shiny gold"), 126);
    }
}
