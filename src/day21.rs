use crate::aocdata;

use itertools::Itertools;
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};

pub fn part1() {
    let data = aocdata::load("data/day21.txt");
    let foods = Foods::from(&data);

    let count = foods.count_non_allergen_ingredients();

    assert_eq!(count, 2280);

    println!("day21 part1: {}", count);
}

pub fn part2() {
    let data = aocdata::load("data/day21.txt");
    let foods = Foods::from(&data);
    let dangerous = foods.canonical_dangerous_ingredients();

    assert_eq!(
        &dangerous,
        "vfvvnm,bvgm,rdksxt,xknb,hxntcz,bktzrz,srzqtccv,gbtmdb"
    );

    println!("day21 part2: {}", dangerous);
}

#[derive(Debug)]
struct Foods {
    foods: Vec<Food>,
}

impl Foods {
    fn from(data: &[String]) -> Self {
        let foods = data.iter().map(|l| Food::from(l)).collect();
        Self { foods }
    }

    // allergen -> Vec<ingredients>
    fn allergen_rough_mapping(&self) -> HashMap<String, Vec<String>> {
        let mut res: HashMap<String, Vec<String>> = HashMap::new();

        for f in &self.foods {
            for a in &f.allergens {
                if res.contains_key(a) {
                    let val = res.get(a).unwrap();
                    let s1: HashSet<String> = val.iter().cloned().collect();
                    let s2: HashSet<_> = f.ingredients.iter().cloned().collect();

                    // intersect old and new
                    let s3: Vec<_> = s1.intersection(&s2).cloned().collect();

                    res.insert(a.to_string(), s3);
                } else {
                    // insert new
                    res.insert(a.clone(), f.ingredients.clone());
                }
            }
        }

        res
    }

    fn filter(map: HashMap<String, Vec<String>>, val: &str) -> HashMap<String, Vec<String>> {
        let mut res = HashMap::new();

        for e in map {
            let key = e.0;
            let val: Vec<String> = e.1.iter().filter(|&v| v != val).cloned().collect();
            if !val.is_empty() {
                res.insert(key, val);
            }
        }

        res
    }

    // allergen -> ingredient
    fn allergen_mapping(&self) -> BTreeMap<String, String> {
        let mut res = BTreeMap::new();

        let mut rough = self.allergen_rough_mapping();
        loop {
            let single = rough.iter().find(|a| a.1.len() == 1);
            if let Some(single) = single {
                let key = single.0;
                let val = single.1[0].clone();

                res.insert(key.clone(), val.clone());

                rough = Self::filter(rough, &val);
            } else {
                panic!("no entry with a single ingredient found!");
            }

            if rough.is_empty() {
                break;
            }
        }

        res
    }

    fn count_non_allergen_ingredients(&self) -> usize {
        let am = self.allergen_mapping();
        let allergen_ingredients: HashSet<&String> = am.values().collect();

        self.foods
            .iter()
            .map(|f| {
                f.ingredients
                    .iter()
                    .filter(|&i| !allergen_ingredients.contains(i))
                    .count()
            })
            .sum()
    }

    fn canonical_dangerous_ingredients(&self) -> String {
        let dangerous = self.allergen_mapping();
        dangerous.iter().map(|d| d.1).join(",")
    }
}

#[derive(Debug)]
struct Food {
    ingredients: Vec<String>,
    allergens: Vec<String>,
}

impl Food {
    // mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
    // trh fvjkl sbzzf mxmxvkd (contains dairy)
    // sqjhc fvjkl (contains soy)
    // sqjhc mxmxvkd sbzzf (contains fish)
    fn from(line: &str) -> Self {
        lazy_static! {
            static ref FOOD_RE: Regex = Regex::new(r"^([a-z ]+) \(contains (.*)+\)$").unwrap();
        }

        let cap = FOOD_RE.captures(&line).unwrap();
        let ing: &str = cap.get(1).unwrap().as_str();
        let all: &str = cap.get(2).unwrap().as_str();

        let ingredients = ing.to_string().split(' ').map(|s| s.to_owned()).collect();
        let allergens = all.to_string().split(", ").map(|s| s.to_owned()).collect();

        Self {
            ingredients,
            allergens,
        }
    }
}
#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day21::Foods;

    #[test]
    fn test() {
        let data = aocdata::load("data/day21ex1.txt");
        let foods = Foods::from(&data);

        println!("{:#?}", foods);

        let rough = foods.allergen_rough_mapping();
        println!("rough {:#?}", rough);

        let mapping = foods.allergen_mapping();
        println!("mapping {:#?}", mapping);

        let count = foods.count_non_allergen_ingredients();
        assert_eq!(count, 5);

        let dangerous = foods.canonical_dangerous_ingredients();
        assert_eq!(&dangerous, "mxmxvkd,sqjhc,fvjkl");
    }
}
