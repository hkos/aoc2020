use crate::aocdata;

use anyhow::Result;
use itertools::Itertools;
use regex::Regex;

use std::collections::HashMap;
use std::str::FromStr;

pub fn part1() {
    let data = aocdata::load("data/day04p1.txt");
    let passports = Passport::from_list(&data);

    println!("day04 part1: {}", passports.len());
}

pub fn part2() {
    let data = aocdata::load("data/day04p1.txt");
    let passports: Vec<_> = Passport::from_list(&data)
        .into_iter()
        .filter(Passport::check_passport)
        .collect();

    println!("day04 part2: {}", passports.len());
}

#[derive(Debug)]
struct Passport {
    byr: String,
    // (Birth Year)
    iyr: String,
    // (Issue Year)
    eyr: String,
    // (Expiration Year)
    hgt: String,
    // (Height)
    hcl: String,
    // (Hair Color)
    ecl: String,
    // (Eye Color)
    pid: String,
    // (Passport ID)
    cid: Option<String>, // (Country ID)
}

impl Passport {
    fn from(data: &[String]) -> Result<Self> {
        const KEYS: &[&str] = &["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"];

        let fields: Vec<_> = data.iter().flat_map(|line| line.split(' ')).collect();

        let fields: HashMap<_, _> = fields.iter().flat_map(|f| f.split(':')).tuples().collect();

        for key in KEYS {
            if !fields.contains_key(key) {
                return Err(anyhow::anyhow!("bad passport, missing {}", key));
            };
        }

        let byr = fields.get("byr").unwrap().to_owned().to_owned();
        let ecl = fields.get("ecl").unwrap().to_owned().to_owned();
        let eyr = fields.get("eyr").unwrap().to_owned().to_owned();
        let hcl = fields.get("hcl").unwrap().to_owned().to_owned();
        let hgt = fields.get("hgt").unwrap().to_owned().to_owned();
        let iyr = fields.get("iyr").unwrap().to_owned().to_owned();
        let pid = fields.get("pid").unwrap().to_owned().to_owned();
        let cid = if let Some(&cid) = fields.get("cid") {
            Some(cid.to_owned())
        } else {
            None
        };

        Ok(Passport {
            byr,
            cid,
            ecl,
            eyr,
            hcl,
            hgt,
            iyr,
            pid,
        })
    }

    fn from_list(data: &[String]) -> Vec<Self> {
        let mut tmp: Vec<String> = vec![];
        let mut res: Vec<Self> = vec![];

        data.iter().for_each(|line| {
            if line.is_empty() {
                if let Ok(p) = Passport::from(&tmp) {
                    res.push(p);
                }
                tmp = vec![];
            } else {
                tmp.push(line.to_owned());
            }
        });

        if !tmp.is_empty() {
            if let Ok(p) = Passport::from(&tmp) {
                res.push(p);
            }
        }

        res
    }

    fn get_year(field: &str) -> Result<u16> {
        if field.len() != 4 {
            return Err(anyhow::anyhow!("year needs 4 digits: '{}'", field));
        };

        if let Ok(year) = u16::from_str(field) {
            Ok(year)
        } else {
            Err(anyhow::anyhow!("bad year field '{}'", field))
        }
    }

    fn check_passport(&self) -> bool {
        // byr (Birth Year) - four digits; at least 1920 and at most 2002.
        if let Ok(year) = Passport::get_year(&self.byr) {
            if !(year >= 1920 && year <= 2002) {
                return false;
            }
        } else {
            return false;
        }

        // iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        if let Ok(year) = Passport::get_year(&self.iyr) {
            if !(year >= 2010 && year <= 2020) {
                return false;
            }
        } else {
            return false;
        }

        // eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        if let Ok(year) = Passport::get_year(&self.eyr) {
            if !(year >= 2020 && year <= 2030) {
                return false;
            }
        } else {
            return false;
        }

        // hgt (Height) - a number followed by either cm or in:
        // If cm, the number must be at least 150 and at most 193.
        // If in, the number must be at least 59 and at most 76.
        let (num, unit) = self.hgt.split_at(self.hgt.len() - 2);
        if let Ok(num) = u16::from_str(num) {
            match unit {
                "cm" => {
                    if num < 150 || num > 193 {
                        return false;
                    }
                }
                "in" => {
                    if num < 59 || num > 76 {
                        return false;
                    }
                }
                _ => return false,
            }
        } else {
            return false;
        }

        // hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        lazy_static! {
            static ref HCL_RE: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
        }
        if !HCL_RE.is_match(&self.hcl) {
            return false;
        }

        // ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        lazy_static! {
            static ref ECL_RE: Regex = Regex::new(r"^amb|blu|brn|gry|grn|hzl|oth$").unwrap();
        }
        if !ECL_RE.is_match(&self.ecl) {
            return false;
        }

        // pid (Passport ID) - a nine-digit number, including leading zeroes.
        lazy_static! {
            static ref PID_RE: Regex = Regex::new(r"^\d{9}$").unwrap();
        }
        if !PID_RE.is_match(&self.pid) {
            return false;
        }

        // cid (Country ID) - ignored, missing or not.

        true
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day04::Passport;

    #[test]
    fn test() {
        let data = aocdata::load("data/day04ex1.txt");
        let passports = Passport::from_list(&data);

        assert_eq!(passports.len(), 2);
    }

    #[test]
    fn test2ok() {
        let data = aocdata::load("data/day04ex2ok.txt");
        let passports = Passport::from_list(&data);

        assert_eq!(passports.len(), 4);

        passports
            .iter()
            .for_each(|p| assert!(Passport::check_passport(p)));
    }

    #[test]
    fn test2bad() {
        let data = aocdata::load("data/day04ex2bad.txt");
        let passports = Passport::from_list(&data);

        assert_eq!(passports.len(), 4);

        passports.iter().for_each(|p| {
            assert!(
                !Passport::check_passport(p),
                format!("should be bad: {:?}", p)
            )
        });
    }
}
