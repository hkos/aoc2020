use crate::aocdata;

use std::collections::HashSet;

pub fn part1() {
    let data = aocdata::load("data/day06p1.txt");
    let count = count_any(&data);

    assert_eq!(count, 6387);

    println!("day06 part1: {}", count);
}

pub fn part2() {
    let data = aocdata::load("data/day06p1.txt");
    let count = count_every(&data);

    assert_eq!(count, 3039);

    println!("day06 part2: {}", count);
}

fn count_block_any(data: &[String]) -> usize {
    let mut chars = HashSet::new();

    data.iter()
        .map(|l| l.chars().into_iter().collect::<HashSet<_>>())
        .for_each(|set| chars = chars.union(&set).copied().collect());

    chars.len()
}

fn count_block_every(data: &[String]) -> usize {
    // initialize to "all chars", because we're using intersections
    let mut chars: HashSet<_> = ('a'..='z').collect();

    data.iter()
        .map(|l| l.chars().into_iter().collect::<HashSet<_>>())
        .for_each(|set| chars = chars.intersection(&set).copied().collect());

    chars.len()
}

fn count(data: &[String], counter: &dyn Fn(&[String]) -> usize) -> usize {
    let blocks = aocdata::get_blocks(data);
    blocks.iter().map(|b| counter(b)).sum()
}

fn count_any(data: &[String]) -> usize {
    count(data, &count_block_any)
}

fn count_every(data: &[String]) -> usize {
    count(data, &count_block_every)
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day06::{count_any, count_every};

    #[test]
    fn test() {
        let data = aocdata::load("data/day06ex1.txt");
        let count = count_any(&data);
        assert_eq!(count, 11);

        let count = count_every(&data);
        assert_eq!(count, 6);
    }
}
