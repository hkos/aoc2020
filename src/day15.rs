use crate::aocdata;
use std::collections::HashMap;

pub fn part1() {
    let data = aocdata::load("data/day15.txt");

    let mut nums = Numbers::new();
    nums.init(&data);

    let res = nums.get2020();
    assert_eq!(res, 260);

    println!("day15 part1: {}", res);
}

pub fn part2() {
    let data = aocdata::load("data/day15.txt");

    let mut nums = Numbers::new();
    nums.init(&data);

    let res = nums.get_n(30000000);
    assert_eq!(res, 950);

    println!("day15 part2: {}", res);
}

#[derive(Debug)]
struct Numbers {
    spoken: Vec<usize>,

    // spoken number -> last occurrence
    cache: HashMap<usize, usize>,
}

impl Numbers {
    fn new() -> Self {
        Self {
            spoken: Vec::new(),
            cache: HashMap::new(),
        }
    }

    fn init(&mut self, line: &[String]) {
        line[0]
            .split(',')
            .for_each(|l| self.put(usize::from_str_radix(l, 10).expect("bad num")));
    }

    fn put(&mut self, n: usize) {
        if let Some(&last) = self.spoken.last() {
            self.cache.insert(last, self.spoken.len() - 1);
        }

        self.spoken.push(n);
    }

    // previous occurrence of "n" (disregarding the last spoken number)
    fn prev(&self, n: usize) -> usize {
        let pos = self.cache.get(&n);

        if let Some(pos) = pos {
            self.spoken.len() - pos - 1
        } else {
            0
        }
    }

    fn next(&mut self) {
        let last = self.spoken.last().unwrap();
        let prev_spoken = self.prev(*last);
        self.put(prev_spoken);
    }

    fn get2020(&mut self) -> usize {
        self.get_n(2020)
    }

    fn get_n(&mut self, n: usize) -> usize {
        while self.spoken.len() < n {
            self.next();
        }

        self.spoken[n - 1]
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day15::Numbers;

    #[test]
    fn test() {
        let data = aocdata::load("data/day15ex1.txt");

        let mut nums = Numbers::new();
        nums.init(&data);

        println!("{:?}", nums);

        let res = nums.get2020();
        assert_eq!(res, 436);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day15ex1.txt");

        let mut nums = Numbers::new();
        nums.init(&data);

        println!("{:?}", nums);

        let res = nums.get_n(30000000);
        assert_eq!(res, 175594);
    }
}
