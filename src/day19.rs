use crate::aocdata;

use regex::Regex;
use std::collections::{HashMap, HashSet};

pub fn part1() {
    let data = aocdata::load("data/day19.txt");
    let input = Input::from(&data);

    let count = input.count_matches();
    assert_eq!(count, 182);

    println!("day19 part1: {}", count);
}

pub fn part2() {
    let data = aocdata::load("data/day19.txt");
    let mut input = Input::from(&data);

    let (_, r8) = Rule::from("8: 42 | 42 8");
    let (_, r11) = Rule::from("11: 42 31 | 42 11 31");
    input.rules.insert(8, r8);
    input.rules.insert(11, r11);

    let count = input.count_matches();
    assert_eq!(count, 334);

    println!("day19 part2: {}", count);
}

#[derive(Debug)]
struct Input {
    rules: HashMap<usize, Rule>,
    messages: Vec<String>,
}

impl Input {
    fn count_matches(&self) -> usize {
        self.messages
            .iter()
            .filter(|&msg| self.match_on_rule0(msg))
            .count()
    }

    // return all possible remainders of "msg", after applying this rule
    fn try_match(&self, msg: &str, rule: &Rule) -> HashSet<String> {
        match rule {
            Rule::Num(n) => self.try_match(msg, self.rules.get(n).unwrap()),
            Rule::Sequence(s) => {
                if let Some(first) = s.get(0) {
                    let res = self.try_match(msg, first);
                    let rest = Rule::Sequence(s[1..].to_vec());
                    res.into_iter()
                        .flat_map(|remainder| self.try_match(&remainder, &rest))
                        .collect()
                } else {
                    vec![msg.to_string()].into_iter().collect()
                }
            }
            Rule::Or(or) => or.iter().flat_map(|r| self.try_match(msg, r)).collect(),
            Rule::Char(c) => match msg.chars().next() {
                Some(ch) if *c == ch => vec![msg[1..].into()].into_iter().collect(),
                _ => HashSet::new(),
            },
        }
    }

    fn match_on_rule0(&self, msg: &str) -> bool {
        let rule0 = self.rules.get(&0).unwrap();

        // a full match has the empty String as a test_match return value
        self.try_match(msg, rule0).contains("")
    }

    fn from(data: &[String]) -> Self {
        let mut lines = data.iter();

        let rules = lines
            .by_ref()
            .take_while(|line| !line.is_empty())
            .map(|line| Rule::from(line))
            .collect();

        let messages = lines.cloned().collect();

        Self { rules, messages }
    }
}

#[derive(Clone, Debug)]
enum Rule {
    Num(usize),
    Or(Vec<Rule>),
    Sequence(Vec<Rule>),
    Char(char),
}

impl Rule {
    fn seq(seq: &str) -> Vec<Rule> {
        let elem = seq.split(' ');
        elem.map(|e| usize::from_str_radix(e, 10).unwrap_or_else(|_| panic!("bad number {}", e)))
            .map(Rule::Num)
            .collect()
    }

    fn from(line: &str) -> (usize, Self) {
        lazy_static! {
            static ref CHAR_RE: Regex = Regex::new(r#"^(\d+): "([ab])"$"#).unwrap();
            static ref SEQ_RE: Regex = Regex::new(r#"^(\d+): ([\d ]+)$"#).unwrap();
            static ref OR_RE: Regex = Regex::new(r#"^(\d+): ([\d ]+) \| ([\d ]+)$"#).unwrap();
        }

        if CHAR_RE.is_match(line) {
            let cap = CHAR_RE.captures(&line).unwrap();
            let n: &str = cap.get(1).unwrap().as_str();
            let n = usize::from_str_radix(n, 10).expect("bad num");
            let ch: &str = cap.get(2).unwrap().as_str();

            (n, Rule::Char(ch.chars().next().unwrap()))
        } else if SEQ_RE.is_match(&line) {
            let cap = SEQ_RE.captures(&line).unwrap();
            let n: &str = cap.get(1).unwrap().as_str();
            let n = usize::from_str_radix(n, 10).expect("bad num");
            let seq: &str = cap.get(2).unwrap().as_str();

            (n, Self::Sequence(Self::seq(seq)))
        } else if OR_RE.is_match(&line) {
            let cap = OR_RE.captures(&line).unwrap();
            let n: &str = cap.get(1).unwrap().as_str();
            let n = usize::from_str_radix(n, 10).expect("bad num");
            let seq1: &str = cap.get(2).unwrap().as_str();
            let seq2: &str = cap.get(3).unwrap().as_str();

            let seq1 = Self::Sequence(Self::seq(seq1));
            let seq2 = Self::Sequence(Self::seq(seq2));

            (n, Self::Or(vec![seq1, seq2]))
        } else {
            panic!(format!("unexpected rule line '{:?}'", line));
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day19::Input;

    #[test]
    fn test() {
        let data = aocdata::load("data/day19ex1.txt");

        let input = Input::from(&data);

        let count = input.count_matches();
        assert_eq!(count, 2);
    }
}
