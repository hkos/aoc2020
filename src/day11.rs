use crate::aocdata;
use crate::area::Field::{Floor, Occupied, Seat};
use crate::area::{Area, Field};

pub fn part1() {
    let data = &aocdata::load("data/day11.txt");
    let area = Area::new(&data);
    let occ = p1(&area);

    assert_eq!(occ, 2453);

    println!("day11 part1: {}", occ);
}

pub fn part2() {
    let data = &aocdata::load("data/day11.txt");
    let area = Area::new(&data);
    let occ = p2(&area);

    assert_eq!(occ, 2159);

    println!("day11 part2: {}", occ);
}

fn p1(area: &Area) -> usize {
    self::run(area, step_p1)
}

fn p2(area: &Area) -> usize {
    self::run(area, step_p2)
}

fn run(area: &Area, step: fn(&Area) -> Area) -> usize {
    let mut prev = area.clone();

    loop {
        let next = step(&prev);

        if next == prev {
            break;
        };
        prev = next;
    }

    prev.flat()
        .iter()
        .filter(|&&f| f == Field::Occupied)
        .count()
}

fn step(area: &Area, f: fn(&Area, x: usize, y: usize) -> Vec<Field>, limit: usize) -> Area {
    let mut next = area.clone();

    let size = area.size();
    for x in 0..size.0 {
        for y in 0..size.1 {
            let a = area.get(x, y);
            if a != Floor {
                let occ_neighbors = f(area, x, y).iter().filter(|&&f| f == Occupied).count();
                if a == Seat && occ_neighbors == 0 {
                    next.set(x, y, Occupied);
                } else if a == Occupied && occ_neighbors >= limit {
                    next.set(x, y, Seat);
                }
            }
        }
    }

    next
}

fn step_p1(area: &Area) -> Area {
    self::step(area, Area::neighbors, 4)
}

fn step_p2(area: &Area) -> Area {
    self::step(area, self::neighbors_p2, 5)
}

// collect the nearest "seat|occupied" Fields in each of the 8 DIRS
fn neighbors_p2(area: &Area, x: usize, y: usize) -> Vec<Field> {
    static DIRS: &[(isize, isize)] = &[
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ];

    let mut res = vec![];

    let size = area.size();
    let size = (size.0 as isize, size.1 as isize);

    'dirs: for dir in DIRS.iter() {
        let mut pos = (x as isize, y as isize);
        loop {
            pos.0 += dir.0;
            pos.1 += dir.1;

            if pos.0 < 0 || pos.1 < 0 || pos.0 >= size.0 || pos.1 >= size.1 {
                continue 'dirs;
            }
            let field = area.get(pos.0 as usize, pos.1 as usize);
            if field == Field::Seat || field == Field::Occupied {
                res.push(field);
                continue 'dirs;
            }
        }
    }

    res
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::area::Area;
    use crate::day11::{p1, p2};

    #[test]
    fn test() {
        let data = &aocdata::load("data/day11ex1.txt");
        let area = Area::new(&data);

        let occ = p1(&area);
        assert_eq!(occ, 37, "p1");

        let occ = p2(&area);
        assert_eq!(occ, 26, "p2");
    }
}
