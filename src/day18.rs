use crate::aocdata;

use regex::Regex;

pub fn part1() {
    let data = aocdata::load("data/day18.txt");

    let sum = p1(&data);
    assert_eq!(sum, 280014646144);

    println!("day18 part1: {}", sum);
}

pub fn part2() {
    let data = aocdata::load("data/day18.txt");

    let sum = p2(&data);
    // assert_eq!(sum, 280014646144);

    println!("day18 part2: {}", sum);
}

fn p1(data: &[String]) -> usize {
    data.iter().map(|l| eval(l)).sum()
}

fn p2(data: &[String]) -> usize {
    data.iter().map(|l| eval2(l)).sum()
}

fn eval(exp: &str) -> usize {
    let mut exp = exp.to_string();

    lazy_static! {
        static ref NUM_LIT_RE: Regex = Regex::new(r"^(\d+)$").unwrap();
    }
    if NUM_LIT_RE.is_match(&exp) {
        return usize::from_str_radix(&exp, 10).expect("bad num");
    }

    lazy_static! {
        static ref PAREN_RE: Regex = Regex::new(r"^.*([(][\d \+\*]+[)]).*$").unwrap();
    }

    while PAREN_RE.is_match(&exp) {
        let cap = PAREN_RE.captures(&exp).unwrap();
        let p: &str = cap.get(1).unwrap().as_str();
        let loc = cap.get(1).unwrap().range();

        let u = eval(&p[1..p.len() - 1]).to_string();

        exp.replace_range(loc, &u);
    }

    lazy_static! {
        static ref LTR_RE: Regex = Regex::new(r"^(.*) ([\+\*]) (\d+)$").unwrap();
    }

    let cap = LTR_RE.captures(&exp).unwrap();
    let a: &str = cap.get(1).unwrap().as_str();
    let a = eval(a);

    let op: &str = cap.get(2).unwrap().as_str();

    let b: &str = cap.get(3).unwrap().as_str();
    let b = usize::from_str_radix(b, 10).expect("bad num");

    match op {
        "+" => a + b,
        "*" => a * b,
        _ => panic!("illegal op"),
    }
}

fn eval2(exp: &str) -> usize {
    let mut exp = exp.to_string();

    lazy_static! {
        static ref NUM_LIT_RE: Regex = Regex::new(r"^(\d+)$").unwrap();
    }
    if NUM_LIT_RE.is_match(&exp) {
        return usize::from_str_radix(&exp, 10).expect("bad num");
    }

    lazy_static! {
        static ref PAREN_RE: Regex = Regex::new(r"^.*([(][\d \+\*]+[)]).*$").unwrap();
    }

    while PAREN_RE.is_match(&exp) {
        let cap = PAREN_RE.captures(&exp).unwrap();
        let p: &str = cap.get(1).unwrap().as_str();
        let loc = cap.get(1).unwrap().range();

        let u = eval2(&p[1..p.len() - 1]).to_string();

        exp.replace_range(loc, &u);
    }

    lazy_static! {
        static ref PLUS_RE: Regex = Regex::new(r"((\d+) \+ (\d+))").unwrap();
    }

    while PLUS_RE.is_match(&exp) {
        let cap = PLUS_RE.captures(&exp).unwrap();

        let loc = cap.get(1).unwrap().range();

        let a: &str = cap.get(2).unwrap().as_str();
        let a = usize::from_str_radix(a, 10).expect("bad num");

        let b: &str = cap.get(3).unwrap().as_str();
        let b = usize::from_str_radix(b, 10).expect("bad num");

        let c = a + b;

        exp.replace_range(loc, &c.to_string());
    }

    lazy_static! {
        static ref MUL_RE: Regex = Regex::new(r"^(.*) \* (\d+)$").unwrap();
    }

    if MUL_RE.is_match(&exp) {
        let cap = MUL_RE.captures(&exp).unwrap();
        let a: &str = cap.get(1).unwrap().as_str();
        let a = eval2(a);

        let b: &str = cap.get(2).unwrap().as_str();
        let b = eval2(b);

        return a * b;
    }

    usize::from_str_radix(&exp, 10).expect("bad num")
}

#[cfg(test)]
mod tests {
    use crate::day18::{eval, eval2};

    #[test]
    fn test() {
        let n = eval("1 + 2 * 3 + 4 * 5 + 6");
        assert_eq!(n, 71);

        let n = eval("1 + (2 * 3) + (4 * (5 + 6))");
        assert_eq!(n, 51);

        let n = eval("2 * 3 + (4 * 5)");
        assert_eq!(n, 26);

        let n = eval("5 + (8 * 3 + 9 + 3 * 4 * 3)");
        assert_eq!(n, 437);

        let n = eval("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))");
        assert_eq!(n, 12240);

        let n = eval("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2");
        assert_eq!(n, 13632);
    }

    #[test]
    fn test2() {
        let n = eval2("1 + 2 * 3 + 4 * 5 + 6");
        assert_eq!(n, 231);

        let n = eval2("1 + (2 * 3) + (4 * (5 + 6))");
        assert_eq!(n, 51);

        let n = eval2("2 * 3 + (4 * 5)");
        assert_eq!(n, 46);

        let n = eval2("5 + (8 * 3 + 9 + 3 * 4 * 3)");
        assert_eq!(n, 1445);

        let n = eval2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))");
        assert_eq!(n, 669060);

        let n = eval2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2");
        assert_eq!(n, 23340);
    }
}
