use crate::aocdata;
use regex::Regex;
use std::collections::{BTreeMap, HashMap, HashSet};

pub fn part1() {
    let data = aocdata::load("data/day16.txt");
    let notes = Notes::from(&data);

    let error_rate = notes.error_rate();

    assert_eq!(error_rate, 28873);

    println!("day16 part1: {}", error_rate);
}

pub fn part2() {
    let data = aocdata::load("data/day16.txt");

    let res = p2(&data);
    assert_eq!(res, 2587271823407);

    println!("day16 part2: {:?}", res);
}

fn p2(data: &[String]) -> usize {
    let notes = Notes::from(&data);

    let x = notes.row_field_mapping();

    let y: HashMap<_, _> = x
        .iter()
        .filter(|(_, s)| s.starts_with("departure"))
        .collect();

    let my = notes.your;

    let mut res = 1;
    y.iter().for_each(|(&&i, _)| res *= my.fields[i]);

    res
}

#[derive(Debug)]
struct Notes {
    fields: HashMap<String, Ranges>,
    your: Ticket,
    nearby: Vec<Ticket>,
}

impl Notes {
    pub fn get_nearby(&self) -> Vec<Ticket> {
        self.nearby.clone()
    }

    pub fn get_nearby_without_invalid(&self) -> Vec<Ticket> {
        let tickets = self.get_nearby();

        tickets
            .iter()
            .filter(|&t| !t.fields.iter().any(|&val| !self.valid_for_any(val)))
            .cloned()
            .collect()
    }

    fn valid_for_any(&self, val: usize) -> bool {
        self.fields.iter().any(|x| x.1.valid(val))
    }

    // remove the field-type "s" from all map entries
    fn strip(map: &BTreeMap<usize, Vec<String>>, s: &str) -> BTreeMap<usize, Vec<String>> {
        let mut res = BTreeMap::new();

        for n in map {
            let filtered: Vec<_> = n.1.iter().filter(|&elem| elem != s).cloned().collect();
            res.insert(*n.0, filtered);
        }

        res
    }

    fn recurse(
        search: &BTreeMap<usize, Vec<String>>,
        ok: HashMap<usize, String>,
    ) -> Option<HashMap<usize, String>> {
        // there's nothing more to recurse into
        if search.is_empty() {
            return Some(ok);
        }

        // fail fast: if there are no possible options for any row
        if search.iter().any(|(_, v)| v.is_empty()) {
            return None;
        }

        // fail fast: if there are fewer field-names than fields left
        let set: HashSet<_> = search.iter().flat_map(|(_, x)| x).collect();
        if set.len() < search.len() {
            return None;
        }

        // fail fast: if there are any two 1-length entries with the same field-name
        let ones: Vec<_> = search
            .iter()
            .filter(|(_, v)| v.len() == 1)
            .map(|(_, v)| v[0].clone())
            .collect();

        let ones_set: HashSet<_> = ones.iter().collect();

        if ones.len() != ones_set.len() {
            return None;
        }

        use std::iter::FromIterator;

        let mut search2 = Vec::from_iter(search.iter());
        search2.sort_by(|&(_, a), &(_, b)| b.len().cmp(&a.len()));

        for (i, v) in search2 {
            let mut next = search.clone();
            next.remove(i);

            for vv in v {
                let mut ok2 = ok.clone();
                ok2.insert(*i, vv.clone());

                // remove vv from all values in next2
                let next2 = Self::strip(&next, vv);

                if let Some(solution) = Self::recurse(&next2, ok2) {
                    return Some(solution);
                }
            }
        }

        None
    }

    fn row_field_mapping(&self) -> HashMap<usize, String> {
        let filtered: Vec<_> = self.get_nearby_without_invalid();

        let possible_fields = self.ticket_rows_possible_fields(&filtered);

        Self::recurse(&possible_fields, HashMap::new()).expect("no solution found")
    }

    fn ticket_rows_possible_fields(
        &self,
        possible_tickets: &[Ticket],
    ) -> BTreeMap<usize, Vec<String>> {
        let row_cols = self.nearby[0].fields.len();

        (0..row_cols)
            .map(|i| (i, self.ticket_row_possible_fields(i, possible_tickets)))
            .collect()
    }

    // check which field-names are possible for 'row' in all tickets
    fn ticket_row_possible_fields(&self, row: usize, possible: &[Ticket]) -> Vec<String> {
        let mut res = vec![];

        for (s, r) in &self.fields {
            if possible.iter().all(|t| r.valid(t.fields[row])) {
                // the field-type is valid for all nearby tickets
                res.push(s.clone());
            }
        }

        res
    }
}

#[derive(Debug)]
struct Ranges {
    a: usize,
    b: usize,
    c: usize,
    d: usize,
}

impl Ranges {
    fn valid(&self, n: usize) -> bool {
        (n >= self.a && n <= self.b) || (n >= self.c && n <= self.d)
    }
}

impl Notes {
    fn error_rate(&self) -> usize {
        // let valid = self.all_valid_fields();
        let mut rate = 0;

        for t in &self.nearby {
            for ticket_field in &t.fields {
                if !self.valid_for_any(*ticket_field) {
                    rate += *ticket_field;
                }
            }
        }

        rate
    }

    fn fields_from(line: &str) -> (String, Ranges) {
        // class: 1-3 or 5-7

        lazy_static! {
            static ref FIELDS_RE: Regex =
                Regex::new(r"^(.*): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();
        }

        let cap = FIELDS_RE.captures(line).unwrap();

        let line = cap.get(1).unwrap().as_str().to_string();
        let a = usize::from_str_radix(cap.get(2).unwrap().as_str(), 10).expect("bad num");
        let b = usize::from_str_radix(cap.get(3).unwrap().as_str(), 10).expect("bad num");
        let c = usize::from_str_radix(cap.get(4).unwrap().as_str(), 10).expect("bad num");
        let d = usize::from_str_radix(cap.get(5).unwrap().as_str(), 10).expect("bad num");

        (line, Ranges { a, b, c, d })
    }

    fn from(data: &[String]) -> Self {
        let mut lines = data.iter();

        let mut fields = HashMap::new();
        loop {
            let line = lines.next().unwrap();
            if line.is_empty() {
                break;
            }

            let entry = Self::fields_from(line);
            fields.insert(entry.0, entry.1);
        }

        let header = lines.next();
        assert_eq!(header, Some(&"your ticket:".to_string()));
        let your = Ticket::from(lines.next().unwrap());

        let empty = lines.next().unwrap();
        assert!(empty.is_empty());
        let header = lines.next().unwrap();
        assert_eq!(header, "nearby tickets:");

        let mut nearby = Vec::new();
        for line in lines {
            nearby.push(Ticket::from(line));
        }

        Self {
            fields,
            your,
            nearby,
        }
    }
}

#[derive(Debug, Clone)]
struct Ticket {
    fields: Vec<usize>,
}

impl Ticket {
    fn from(line: &str) -> Self {
        let fields = line
            .split(',')
            .into_iter()
            .map(|n| usize::from_str_radix(n, 10).expect("bad number"))
            .collect();
        Self { fields }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day16::Notes;
    use std::collections::HashMap;

    #[test]
    fn test() {
        let data = aocdata::load("data/day16ex1.txt");
        let notes = Notes::from(&data);

        let error_rate = notes.error_rate();

        assert_eq!(error_rate, 71);
    }

    #[test]
    fn test2() {
        let data = aocdata::load("data/day16ex2.txt");
        let notes = Notes::from(&data);

        let mapping = notes.row_field_mapping();

        let expected: HashMap<usize, String> = [(0, "row"), (1, "class"), (2, "seat")]
            .iter()
            .map(|(u, s)| (*u as usize, s.to_string()))
            .collect();

        assert_eq!(mapping, expected);
    }
}
