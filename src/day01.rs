use crate::aocdata;

pub fn part1() {
    let data = aocdata::as_u64(&aocdata::load("data/day01p1.txt"));
    println!("day01 part1: {}", p1(&data));
}

pub fn part2() {
    let data = aocdata::as_u64(&aocdata::load("data/day01p1.txt"));
    println!("day01 part2: {}", p2(&data));
}

fn p1(data: &[u64]) -> u64 {
    let (a, b) = pair2020(data);
    a * b
}

fn p2(data: &[u64]) -> u64 {
    let (a, b, c) = triplet2020(data);
    a * b * c
}

fn pair2020(data: &[u64]) -> (u64, u64) {
    for (i, a) in data.iter().enumerate() {
        for b in data[i..].iter() {
            if a + b == 2020 {
                return (*a, *b);
            }
        }
    }

    panic!("no pair found");
}

fn triplet2020(data: &[u64]) -> (u64, u64, u64) {
    for (i, a) in data.iter().enumerate() {
        for (j, b) in data[i + 1..].iter().enumerate() {
            for c in data[j + 1..].iter() {
                if a + b + c == 2020 {
                    return (*a, *b, *c);
                }
            }
        }
    }

    panic!("no triplet found");
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day01::{p1, p2};

    #[test]
    fn test() {
        let data = &aocdata::as_u64(&aocdata::load("data/day01ex1.txt"));

        assert_eq!(p1(data), 514579);
        assert_eq!(p2(data), 241861950);
    }
}
