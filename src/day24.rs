use crate::aocdata;

use std::collections::HashSet;
use std::slice::Iter;

pub fn part1() {
    let data = aocdata::load("data/day24.txt");

    let tiles = Tiles::from(&data);
    let black = tiles.count_black();

    assert_eq!(black, 469);

    println!("day24 part1: {}", black);
}

pub fn part2() {
    let data = aocdata::load("data/day24.txt");

    let tiles = Tiles::from(&data);
    let day100 = tiles.step_n(100);

    assert_eq!(day100, 4353);

    println!("day24 part2: {}", day100);
}

struct Tiles {
    black: HashSet<Coord>,
}

impl Tiles {
    fn new() -> Self {
        Self {
            black: HashSet::new(),
        }
    }

    fn from(data: &[String]) -> Self {
        let mut tiles = Tiles::new();

        for line in data {
            let c = Coord::from_path(&line);
            tiles.flip(&c);
        }

        tiles
    }

    fn flip(&mut self, coord: &Coord) {
        if self.black.contains(coord) {
            self.black.remove(coord);
        } else {
            self.black.insert(*coord);
        }
    }

    fn count_black(&self) -> usize {
        self.black.len()
    }

    fn black_neighbors(&self, coord: &Coord) -> usize {
        Dir::iter()
            .map(|d| d.goto(coord))
            .filter(|n| self.black.contains(n))
            .count()
    }

    fn all_neighbors_of_black_tiles(&self) -> HashSet<Coord> {
        let mut neighbors: HashSet<Coord> = HashSet::new();

        for c in &self.black {
            for n in c.neighbors().iter() {
                neighbors.insert(*n);
            }
        }

        neighbors
    }

    fn step(&self) -> Self {
        let consider = self.all_neighbors_of_black_tiles();

        let mut next = Tiles::new();

        for c in consider {
            let neigh = self.black_neighbors(&c);
            match (self.black.contains(&c), neigh) {
                (true, i) if i == 1 || i == 2 => {
                    let _ = next.black.insert(c);
                }
                (false, 2) => {
                    let _ = next.black.insert(c);
                }
                _ => {}
            }
        }

        next
    }

    fn step_n(mut self, n: usize) -> usize {
        for _ in 1..=n {
            self = self.step();
        }

        self.count_black()
    }
}

#[derive(PartialEq, Eq, Hash, Copy, Clone, Debug)]
struct Coord(i32, i32);

impl Coord {
    fn from_path(path: &str) -> Self {
        let mut pos = Coord(0, 0);

        let mut iter = path.chars();

        while let Some(c) = iter.next() {
            let dir = if c == 'n' || c == 's' {
                let d = iter.next();
                c.to_string() + &d.unwrap().to_string()
            } else {
                c.to_string()
            };

            let dir = Dir::from(&dir);

            pos = dir.goto(&pos);
        }

        pos
    }

    fn neighbors(&self) -> [Self; 6] {
        [
            Dir::E.goto(self),
            Dir::W.goto(self),
            Dir::SE.goto(self),
            Dir::SW.goto(self),
            Dir::NE.goto(self),
            Dir::NW.goto(self),
        ]
    }
}

#[derive(Debug)]
enum Dir {
    E,
    SE,
    SW,
    W,
    NW,
    NE,
}

impl Dir {
    fn iter() -> Iter<'static, Self> {
        [Self::E, Self::W, Self::SE, Self::SW, Self::NE, Self::NW].iter()
    }

    fn goto(&self, c: &Coord) -> Coord {
        match self {
            Self::E => Coord(c.0 + 10, c.1),
            Self::W => Coord(c.0 - 10, c.1),
            Self::SW => Coord(c.0 - 5, c.1 - 10),
            Self::SE => Coord(c.0 + 5, c.1 - 10),
            Self::NW => Coord(c.0 - 5, c.1 + 10),
            Self::NE => Coord(c.0 + 5, c.1 + 10),
        }
    }

    fn from(d: &str) -> Self {
        match d {
            "e" => Self::E,
            "w" => Self::W,
            "nw" => Self::NW,
            "ne" => Self::NE,
            "sw" => Self::SW,
            "se" => Self::SE,
            _ => panic!("unexpected dir"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::aocdata;
    use crate::day24::Tiles;

    #[test]
    fn test() {
        let data = aocdata::load("data/day24ex1.txt");

        let tiles = Tiles::from(&data);
        assert_eq!(tiles.count_black(), 10);

        let day100 = tiles.step_n(100);

        assert_eq!(day100, 2208);
    }
}
