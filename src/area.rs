use std::fmt::{Display, Formatter};

#[derive(Debug, PartialEq, Clone)]
pub struct Area {
    fields: Vec<Vec<Field>>,
}

impl Area {
    pub fn new(data: &[String]) -> Self {
        let mut fields = vec![];

        for line in data {
            let mut l = vec![];
            for f in line.chars() {
                l.push(Field::from(f));
            }
            fields.push(l);
        }

        Self { fields }
    }

    pub fn size(&self) -> (usize, usize) {
        (self.fields[0].len(), self.fields.len())
    }

    pub fn get(&self, x: usize, y: usize) -> Field {
        self.fields[y][x]
    }

    pub fn set(&mut self, x: usize, y: usize, f: Field) {
        self.fields[y][x] = f;
    }

    pub fn neighbors(&self, x: usize, y: usize) -> Vec<Field> {
        let mut res = vec![];
        let size = self.size();

        for i in (if x > 1 { x - 1 } else { 0 })..=usize::min(x + 1, size.0 - 1) {
            for j in (if y > 1 { y - 1 } else { 0 })..=usize::min(y + 1, size.1 - 1) {
                if !(i == x && j == y) {
                    res.push(self.get(i, j));
                }
            }
        }

        res
    }

    pub fn flat(&self) -> Vec<Field> {
        self.fields.iter().flat_map(|l| l.iter()).cloned().collect()
    }
}

impl Display for Area {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), std::fmt::Error> {
        for line in &self.fields {
            for f in line {
                fmt.write_str(&f.to().to_string())?;
            }
            fmt.write_str("\n")?;
        }

        Ok(())
    }
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub enum Field {
    Floor,
    Seat,
    Occupied,
}

impl Field {
    fn from(c: char) -> Self {
        match c {
            '.' => Field::Floor,
            'L' => Field::Seat,
            '#' => Field::Occupied,
            _ => unimplemented!(),
        }
    }

    fn to(&self) -> char {
        match self {
            Field::Floor => '.',
            Field::Seat => 'L',
            Field::Occupied => '#',
        }
    }
}
